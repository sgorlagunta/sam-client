import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {DropdownModule} from "ng2-dropdown";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  username: String;
  password: String;

  constructor(
    private authService:AuthService,
    private router:Router,
    private flashMessage:FlashMessagesService) { }

  ngOnInit() {
  }

  onHome() {
    //console.log("from navbar component");

  var temp = (<HTMLInputElement>document.getElementById('loggedmanager')).value;
  console.log("from navbar::home component::"+temp)
  this.router.navigate(['/'], { queryParams: { user: temp } });
}

onDelegate(){
  var temp = this.authService.getUser();
  console.log("from navbar::home component::"+temp)
  this.router.navigate(['/delegate'], { queryParams: { user: temp } });
}


onClient() {
  //console.log("from navbar component");

var temp = (<HTMLInputElement>document.getElementById('loggedmanager')).value;
console.log("from navbar:; primary component::"+temp)
this.router.navigate(['/output']);
// this.router.navigate(['/output'], { queryParams: { user: temp } });
//<input type="text" name="loggedmanager" value={{user}}/>
  /*const user = {
    username: this.username,
    password: this.password
  }

  this.authService.authenticateUser(user).subscribe(data => {
    if(data.success){
      this.authService.storeUserData(data.token, data.user);
      console.log("length::"+user.username);
      //console.log("username::"+data.user.username);
      //console.log("manager::"+data.user.manager);
      this.flashMessage.show('You are now logged in', {
        cssClass: 'alert-success',
        timeout: 5000});
      //this.router.navigate(['output']);
      this.router.navigate(['/output'], { queryParams: { user: user.username } });
    }
  }); */
  //this.router.navigate(['/output']);
}

  onLogoutClick(){
    this.authService.logout();
    this.flashMessage.show('You are logged out', {
      cssClass:'alert-success',
      timeout: 3000
    });
    this.router.navigate(['/login']);
    return false;
  }

  close(){
    var win = window.open("/","_self"); /* url = “” or “about:blank”; target=”_self” */
    win.close();
  }
}
