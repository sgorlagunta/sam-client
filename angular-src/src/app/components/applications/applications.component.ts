import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
//import component, ElementRef, input and the oninit method from angular core
import { Component, OnInit, ElementRef, Input } from '@angular/core';
//import the file-upload plugin
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
//import the native angular http and respone libraries
import { Http, Response } from '@angular/http';
//import the do function to be used with the http library.
import "rxjs/add/operator/do";
//import the map function to be used with the http library
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';

import {Ng2PaginationModule} from 'ng2-pagination';

import swal from 'sweetalert2/dist/sweetalert2.all.min.js';
import { ExcelService } from '../../services/excel.service';

const URL = 'http://localhost:8080/api/upload';



@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
    //This is the default title property created by the angular cli. Its responsible for the app works
    title = 'Applications In Scope for Recertification';
    //primaryapplications:Object;
    primaryapplications:any[];
    primary:any[];


    ngOnInit() {


         //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
         this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
       //overide the onCompleteItem property of the uploader so we are
       //able to deal with the server response.
         this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
               console.log("ImageUpload:uploaded:", item, status, response);
               var responsePath = JSON.parse(response);
               console.log("ImageUpload--> completion:",response, responsePath);// the url will be in the response
               this.flashMessage.show('Files uploaded successfully', {cssClass: 'alert-success', timeout: 3000});
               //this.router.navigate(['/applications']);
                window.location.reload();
           };

           /*this.uploader.onCompleteAll = function() {
              console.info('onCompleteAll');
              window.location.reload();
            }; */
       }



       //declare a constroctur, so we can pass in some properties to the class, which can be    //accessed using the this variable
         constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private apiService:ApiService,
         private excelService: ExcelService,
         private router: Router) {
           this.getApplications();
           this.excelService = excelService;
         }
         //the function which handles the file upload without using a plugin.
         upload() {
         //locate the file element meant for the file upload.
             let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
         //get the total amount of files attached to the file input.
             let fileCount: number = inputEl.files.length;
         //create a new fromdata instance
             let formData = new FormData();
         //check if the filecount is greater than zero, to be sure a file was selected.
             if (fileCount > 0) { // a file was selected
                 //append the key name 'photo' with the first file in the element
                     formData.append('photo', inputEl.files.item(0));
                 //call the angular http method
                 this.http
             //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                     .post(URL, formData).map((res:Response) => res.json()).subscribe(
                     //map the success function and alert the response
                      (success) => {
                              alert(success._body);
                     },
                     (error) => alert(error))
               }

            }

            getApplications(){
                this.apiService.getApplications()
                    .subscribe(primaryapplications=>{
                      this.primaryapplications = primaryapplications;
                    /*  this.primary = this.primaryapplications;



                      for (var key in this.primary){
                        //console.log( "this.apps: keys"+key, this.apps[key] );
                        delete this.primary[key]._id;
                        //console.log( "this.apps: keys"+key, this.apps[key] );
                      } */
                  });
              }

              deleteApplication(id){
        if(confirm("Are you sure to delete application ")) {
              console.log("Implement delete functionality here");
              this.apiService.deleteApplication(id)
              .subscribe(()=>{
                this.getApplications();

            });

     }
   }


     getDeletion(id){
       this.apiService.deleteApplication(id)
       .subscribe(()=>{
         this.getApplications();
       });
     }

     download()
     {
       var htmltable= document.getElementById('report');
       var html = htmltable.innerHTML;
       window.open('data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html));
       //window.open('data:text/csv;charset=utf-8;' + encodeURIComponent(html));
//text/csv;charset=utf-8;
        //var blob = new Blob([document.getElementById('report').innerHTML], {
        //  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8" });
        //  saveAs(blob, "report.xls");
       console.log("download button clicked");
     }

     exportToExcel(event) {
       this.excelService.exportAsExcelFile(this.primaryapplications, 'primaryapplications');
     }


}
