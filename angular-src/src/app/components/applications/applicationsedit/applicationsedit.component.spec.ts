import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationseditComponent } from './applicationsedit.component';

describe('ApplicationseditComponent', () => {
  let component: ApplicationseditComponent;
  let fixture: ComponentFixture<ApplicationseditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationseditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationseditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
