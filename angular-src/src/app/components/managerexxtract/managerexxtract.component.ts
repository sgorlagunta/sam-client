import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {ManagerExtract} from '../../models/managerextract';
import {ManagerextractService} from '../../services/managerextract.service';
import { ExcelService } from '../../services/excel.service';


const URL = 'http://localhost:8080/api/managerextract';

@Component({
  selector: 'app-managerexxtract',
  templateUrl: './managerexxtract.component.html',
  styleUrls: ['./managerexxtract.component.css']
})
export class ManagerexxtractComponent implements OnInit {

  public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
  title = 'Manager Extracts';

  //primarymanagers:ManagerExtract;
  primarymanagers:any[];
  primary:any[];

  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         private excelService: ExcelService,
         private router: Router) {
          // this.getManagerExtracts();
           this.excelService = excelService;
         }
  ngOnInit() {
           //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
           this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
         //overide the onCompleteItem property of the uploader so we are
         //able to deal with the server response.
           this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
                 console.log("ImageUpload:uploaded:", item, status, response);
                 var responsePath = JSON.parse(response);
                 console.log("ImageUpload--> completion:",response, responsePath);// the url will be in the response
                 this.flashMessage.show('Files uploaded successfully', {cssClass: 'alert-success', timeout: 3000});
                 //this.router.navigate(['/managerextracts']);
                  //window.location.reload();
             };

             this.uploader.onCompleteAll = function() {
                console.info('onCompleteAll');
                window.location.reload();
              };
         }

         //the function which handles the file upload without using a plugin.
         upload() {
         //locate the file element meant for the file upload.
             let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
         //get the total amount of files attached to the file input.
             let fileCount: number = inputEl.files.length;
         //create a new fromdata instance
             let formData = new FormData();
         //check if the filecount is greater than zero, to be sure a file was selected.
             if (fileCount > 0) { // a file was selected
                 //append the key name 'photo' with the first file in the element
                     formData.append('photo', inputEl.files.item(0));
                 //call the angular http method
                 this.http
             //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                     .post(URL, formData).map((res:Response) => res.json()).subscribe(
                     //map the success function and alert the response
                      (success) => {
                              alert(success._body);
                                //this.clearSearch();
                              //window.location.reload();
                            //))
                     },
                     (error) => alert(error))
               }
               //window.location.reload();

            }
            clearSearch()
            {
              window.location.reload();
            }

          /*  getManagerExtracts(){
                this.managerextractService.getManagerExtracts()
                    .subscribe(primarymanagers=>{
                      this.primarymanagers = primarymanagers;
                      this.primary = primarymanagers;

                      for (var key in this.primary){
                        //console.log( "this.apps: keys"+key, this.apps[key] );
                        delete this.primary[key]._id;
                        //console.log( "this.apps: keys"+key, this.apps[key] );
                      }
                  });
              } */

              exportToExcel(event) {
                this.excelService.exportAsExcelFile(this.primary, 'primarymanagers');
              }

}
