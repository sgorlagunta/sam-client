import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {UserExtract} from '../../models/userextract';

import {UserextractService} from '../../services/userextract.service';
import { ExcelService } from '../../services/excel.service';
import {AuthService} from '../../services/auth.service';


const URL = 'http://localhost:8080/api/userextract';

@Component({
  selector: 'app-userextract',
  templateUrl: './userextract.component.html',
  styleUrls: ['./userextract.component.css']
})
export class UserextractComponent implements OnInit {

  public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
  title = 'Application Users';


primaryusers:any[];//UserExtract;
 //persons: Person[];

    ngOnInit() {
           //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
           this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
         //overide the onCompleteItem property of the uploader so we are
         //able to deal with the server response.
           this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
                 console.log("ImageUpload:uploaded:", item, status, response);
                 var responsePath = JSON.parse(response);
                 console.log("ImageUpload--> completion:",response, responsePath);// the url will be in the response
                 this.flashMessage.show('Files uploaded successfully', {cssClass: 'alert-success', timeout: 3000});
                 this.router.navigate(['/userextracts']);
                  //window.location.reload();
             };

             this.uploader.onCompleteAll = function() {
                console.info('onCompleteAll');
                window.location.reload();
              };
         }

         constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private userextractService:UserextractService,
         private excelService: ExcelService,
         private authuser: AuthService,
         private router: Router) {
           this.getUsers();
           this.excelService = excelService;
          // this.persons = PERSONS;
           //this.primaryusers = primaryusers;
         }
        

         //the function which handles the file upload without using a plugin.
         upload() {
         //locate the file element meant for the file upload.
             let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
         //get the total amount of files attached to the file input.
             let fileCount: number = inputEl.files.length;
         //create a new fromdata instance
             let formData = new FormData();
         //check if the filecount is greater than zero, to be sure a file was selected.
             if (fileCount > 0) { // a file was selected
                 //append the key name 'photo' with the first file in the element
                     formData.append('photo', inputEl.files.item(0));
                 //call the angular http method
                 this.http
             //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                     .post(URL, formData).map((res:Response) => res.json()).subscribe(
                     //map the success function and alert the response
                      (success) => {
                              alert(success._body);
                     },
                     (error) => alert(error))
               }

            }

            getUsers(){
                this.userextractService.getUsers()
                    .subscribe(primaryusers=>{
                      this.primaryusers = primaryusers;
                  });
              }

              download()
              {
                var htmltable= document.getElementById('report1');
                var html = htmltable.innerHTML;
                window.open('data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html));
                console.log("download button clicked");
              }

              exportToExcel(event) {
                this.excelService.exportAsExcelFile(this.primaryusers, 'primaryusers');
              }
             
}
