import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {UploadmanagerService} from '../../services/uploadmanager.service';
import { Uploadmanager } from './uploadmanager';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-managerdashboard',
  templateUrl: './managerdashboard.component.html',
  styleUrls: ['./managerdashboard.component.css']
})
export class ManagerdashboardComponent implements OnInit {

  constructor( public uploadmanagerService:UploadmanagerService,
   public route:ActivatedRoute,
   private flashMessage:FlashMessagesService,
   public router:Router) {
   //this.route.params.subscribe( params => console.log(params) );
 }

   private sub: any;
   private user: any;

  ngOnInit() {
     //this.getUploadmanagers();
     //console.log(user);

     var url = document.location.href;

     var temp = url.split('?');

     //temp = temp.split('='); // Now we have all the values in an array, with each element looking like "var=val";

     for(var i=0; i<temp.length; i++)
     {
       var qval2 = temp[i].split("=");
       var qval = qval2[i];

       qval = qval.replace("%40", "@");
          console.log(qval);
          this.user = qval;
       //document.getElementById(qval[0]).value = qval[1];
     }
     console.log("this.user::"+this.user);
   }

 //ngOnDestroy() {
//   this.sub.unsubscribe();
 //}

 

 primarymanagers:Uploadmanager;
 getUploadmanagers(){
   this.uploadmanagerService.getUploadmanagers()
       .subscribe(primarymanagers=>{
         this.primarymanagers = primarymanagers;
         console.log(this.primarymanagers);
       });
 }

}
