import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {ManagerExtract} from '../../../models/managerextract';
import {ManagerextractService} from '../../../services/managerextract.service';
import {ManagerChange} from '../../../models/managerchange';

@Component({
  selector: 'app-managerchangeedit',
  templateUrl: './managerchangeedit.component.html',
  styleUrls: ['./managerchangeedit.component.css']
})
export class ManagerchangeeditComponent implements OnInit {

  constructor(private http: Http, private el: ElementRef,
        public route:ActivatedRoute,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         private router: Router) {
           this.getManager();
         }

  ngOnInit() {
    this.getManager();
  }
  id = this.route.snapshot.params['id'];
  title = 'Update Manager Email ID';
  manager = new ManagerChange;
  getManager(){
      this.managerextractService.getManager(this.id)
          .subscribe(manager=>{
            this.manager = manager;
          });
    }

    updateManager(){
      this.managerextractService.updateManager(this.id,this.manager)
          .subscribe(()=> this.goBack());
           this.flashMessage.show('Manager updated successfully', {cssClass: 'alert-success', timeout: 3000});
    }

     goBack(){
      this.router.navigate(['/managerchange'])
    }

}
