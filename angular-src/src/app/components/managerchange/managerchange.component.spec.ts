import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerchangeComponent } from './managerchange.component';

describe('ManagerchangeComponent', () => {
  let component: ManagerchangeComponent;
  let fixture: ComponentFixture<ManagerchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
