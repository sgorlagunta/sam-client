import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {ManagerExtract} from '../../models/managerextract';
import {ManagerextractService} from '../../services/managerextract.service';
import {ManagerChange} from '../../models/managerchange';
import { ExcelService } from '../../services/excel.service';

@Component({
  selector: 'app-managerchange',
  templateUrl: './managerchange.component.html',
  styleUrls: ['./managerchange.component.css']
})
export class ManagerchangeComponent implements OnInit {

  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         private excelService: ExcelService,
         private router: Router) {
           this.getManagers();
           this.excelService = excelService;
         }

  ngOnInit() {
  }

  //listmanagers:ManagerChange;
  listmanagers:any[];
  list:any[];

getManagers(){
  this.managerextractService.getManagers()
      .subscribe(listmanagers=>{
        this.listmanagers = listmanagers;
      /*  this.list = listmanagers;
        for (var key in this.list){
          //console.log( "this.apps: keys"+key, this.apps[key] );
          delete this.list[key]._id;
          //console.log( "this.apps: keys"+key, this.apps[key] );
        }*/
        //console.log(listmanagers);

      });
}

exportToExcel(event) {
  this.excelService.exportAsExcelFile(this.listmanagers, 'listmanagers');
}

}
