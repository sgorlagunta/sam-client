import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {ManagerExtract} from '../../models/managerextract';
import {ManagerextractService} from '../../services/managerextract.service';
import {ManagerChange} from '../../models/managerchange';
import { ExcelService } from '../../services/excel.service';

@Component({
  selector: 'app-delegatemanager',
  templateUrl: './delegatemanager.component.html',
  styleUrls: ['./delegatemanager.component.css']
})
export class DelegatemanagerComponent implements OnInit {

 user: any;
  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         private excelService: ExcelService,
         private router: Router) {
           this.getManagers();

           this.excelService = excelService;
           var url = document.location.href;
           var temp = url.split('?');
           for(var i=0; i<temp.length; i++)
           {
             var qval2 = temp[i].split("=");
             var qval = qval2[i];
             qval = qval.replace("%40", "@");
               // console.log(qval);
                this.user = qval;
             //document.getElementById(qval[0]).value = qval[1];
           }
           this.getDelegateManager();

         }

  ngOnInit() {
  }

  //listmanagers:ManagerChange;
  listmanagers:any[];
  list:any[];
  dmanager:any[];

getManagers(){
  this.managerextractService.getManagers()
      .subscribe(listmanagers=>{
        this.listmanagers = listmanagers;
      /*  this.list = listmanagers;
        for (var key in this.list){
          //console.log( "this.apps: keys"+key, this.apps[key] );
          delete this.list[key]._id;
          //console.log( "this.apps: keys"+key, this.apps[key] );
        }*/
        //console.log(listmanagers);

      });
}
updateManager(){
  var oldmanager = (<HTMLInputElement>document.getElementById('loggedmanager')).value;
  console.log("delegate Manager form submit::"+oldmanager);
  var newmanager = (<HTMLInputElement>document.getElementById('delegatemanagerName')).value;
  console.log("delegate Manager form submit::"+newmanager);
    this.managerextractService.delegateManagers(oldmanager,newmanager)
        .subscribe(()=> this.goBack());
         this.flashMessage.show('Delegation updated successfully', {cssClass: 'alert-success', timeout: 3000});

}

exportToExcel(event) {
  this.excelService.exportAsExcelFile(this.listmanagers, 'listmanagers');
}

goBack(){
    //window.location.reload();
//this.router.navigate(['/delegatemanager'])
}

goBackScreen(){
  window.history.back();
}


deleteDelegation()
{
  var oldmanager = (<HTMLInputElement>document.getElementById('loggedmanager')).value;
  console.log("delegate Manager form submit::"+oldmanager);
  var newmanager = " ";
  console.log("delegate Manager form submit::"+newmanager);
    this.managerextractService.deleteDelegateManagers(oldmanager)
        .subscribe(()=> this.goBack());
         this.flashMessage.show('Delegation deleted successfully', {cssClass: 'alert-success', timeout: 3000});
         (<HTMLInputElement>document.getElementById('delegatemanagerName')).value = "";
}

getDelegateManager()
{
  console.log("this.user::"+this.user);
  this.managerextractService.getDelegateManager(this.user)
      .subscribe(listmanagers=>{
        this.listmanagers = listmanagers;
        console.log("this.listmanagers::"+this.listmanagers);
        //if(this.listmanagers["_id"]!=null)
        if(this.listmanagers){
        for (var key in this.listmanagers){
          console.log( "this.listmanagers: keys"+key, this.listmanagers[key] );
          if(this.listmanagers[key].length<1)
          this.listmanagers[key] = "";
        }
        this.dmanager = this.listmanagers["dmanager"];
        console.log("damanger:"+this.dmanager);
      }else{
        //var msg =
        //document.getElementById('delegatemanagerName').color.disabled=true;
        (<HTMLInputElement>document.getElementById('delegatemanagerName')).value = "This option is available for Managers only";
        (<HTMLInputElement>document.getElementById('delegatemanagerName')).disabled = true;
        (<HTMLInputElement>document.getElementById('save')).disabled = true;
        (<HTMLInputElement>document.getElementById('remove')).disabled = true;
      }

  });

}

}
