import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelegatemanagerComponent } from './delegatemanager.component';

describe('DelegatemanagerComponent', () => {
  let component: DelegatemanagerComponent;
  let fixture: ComponentFixture<DelegatemanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelegatemanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelegatemanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
