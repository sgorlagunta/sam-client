import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';
import {Ng2PaginationModule} from 'ng2-pagination';
import { DashboardService } from '../../services/dashboard.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import { ExcelService } from '../../services/excel.service';
import { UsersDashboard,Dashboard } from './dashboard';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

   //usersDashboard:Object;
   usersDashboard:any[];//UsersDashboard;
   apps:any[];
   apps2:any[];//Dashboard;
   //Dashboard;//any[];
   //apps2 = new Dashboard();
   //apps2:Object;


 ngOnInit() {
 }
  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private dashboardService:DashboardService,
         private excelService: ExcelService,
         private router: Router) {
           this.getUsersDashboard();
           this.excelService = excelService;
         }

         getUsersDashboard(){
                this.dashboardService.getUsersDashboard()
                    .subscribe(usersDashboard=>{

                      var len = usersDashboard.length;
                      console.log("Implement filtering functionality here"+usersDashboard.length);
                      var result = [];
                      var item = [];
                      var result = [];

                      var application = [];
                      var total = new Array(len).fill(0);//[] = 0;// [0];
                      var recertified= new Array(len).fill(0);
                      var expired= new Array(len).fill(0);
                      var pending= new Array(len).fill(0);
                      var removals= new Array(len).fill(0);
                      var leavers= new Array(len).fill(0);
                      var noChange= new Array(len).fill(0);
                      var movedToSpecial= new Array(len).fill(0);

                      var ttotal = 0;
                      var trecertified= 0;
                      var texpired= 0;
                      var tpending= 0;
                      var tremovals= 0;
                      var tleavers= 0;
                      var tnoChange= 0;
                      var tmovedToSpecial= 0;


                      //var apps2 = new Dashboard();

                      var p = 0;
                      for(var k=0;k<len;k++) {
                        item[k] = usersDashboard[k].application;
                        //console.log(usersDashboard[k].application);
                      }
                      // console.log(" item length"+len);

                       for(var i=0;i<len;i++){
                                  var isDistinct = false;
                                  for(var j=0;j<i;j++){
                                      if(item[i] == item[j]){
                                          isDistinct = true;
                                          break;
                                      }
                                  }
                                  if(!isDistinct){
                                    result[p++] = item[i];
                                    //  console.log(item[i]+" ");
                                  }
                              }


                      for(var i=0;i<result.length;i++){
                          var appl;
                          var sta;
                          for(var k=0;k<len;k++) {
                           appl = usersDashboard[k].application;
                           sta = usersDashboard[k].status;
                             if(result[i] == appl )
                             {
                               total[i]++;ttotal++;
                               if(sta == 'Expired') {
                                expired[i]++;texpired++;recertified[i]++;trecertified++;}
                               else  if(sta == 'Not Recertified') {
                                pending[i]++; tpending++; }
                               else  if(sta == 'Leavers') {
                                leavers[i]++;tleavers++;recertified[i]++;trecertified++; }
                               else  if(sta == 'Removals') {
                                removals[i]++;tremovals++;recertified[i]++;trecertified++;}
                               else  if(sta == 'No Change') {
                                noChange[i]++;tnoChange++;recertified[i]++;trecertified++;}
                               else  if(sta == 'Moved to Special Cycle') {
                                movedToSpecial[i]++;tmovedToSpecial++;recertified[i]++;trecertified++;}
                                //else  if(sta != 'Not Recertified') {
                                 //recertified[i]++;trecertified++;}
                             }
                           }
                        }

                        delete usersDashboard.manager;
                        usersDashboard["manager"]=null;
                        delete usersDashboard["manager"];

                        //console.log(usersDashboard.manager);
                        this.apps = usersDashboard;
                        console.log("  this.apps :: keys"  +this.apps.keys());



                        for (let i in this.apps) {
                          this.apps[i]._id = undefined;
                          this.apps[i].manager = undefined;
                          this.apps[i].logonid = undefined;
                          this.apps[i].status = undefined;
                          this.apps[i].surname = undefined;
                          this.apps[i].initials = undefined;
                          this.apps[i].userlocation = undefined;
                          this.apps[i].comments = undefined;
                          this.apps[i].application = undefined;
                        }
                        for (var key in this.apps){
                          //console.log( "this.apps: keys"+key, this.apps[key] );
                          delete this.apps[key]._id;
                          delete this.apps[key].manager;
                          delete this.apps[key].logonid;
                          delete this.apps[key].status;
                          delete this.apps[key].surname;
                          delete this.apps[key].initials;
                          delete this.apps[key].userlocation;
                          delete this.apps[key].comments;
                          delete this.apps[key].application;
                          //console.log( "this.apps: keys"+key, this.apps[key] );
                        }

                        //console.log(delete this.apps["manager"]);

                        /*delete this.apps["manager"];
                        this.apps["manager"]=null;
                        this.apps["_id"] = null;
                        this.apps["logonid"] = null;
                        this.apps["status"] = null;
                        this.apps["surname"] = null;
                        this.apps["initials"] = null;
                        this.apps["userlocation"] = null;
                        this.apps["comments"] = null;*/


                        //this.apps = [];
                        this.apps.length=(result.length+1);
                        var t = 0;
                        for (let i in this.apps) {
                        t++;
                        if(t <= result.length)
                        {
                            this.apps[i].application = result[i];
                            this.apps[i].total = total[i];
                            this.apps[i].pending = pending[i];
                            this.apps[i].recertified = recertified[i];
                            this.apps[i].expired = expired[i];
                            this.apps[i].removals = removals[i];
                            this.apps[i].leavers = leavers[i];                    
                            this.apps[i].noChange = noChange[i];
                            this.apps[i].movedToSpecial = movedToSpecial[i];
                          /*  this.apps[i]._id = undefined;
                            this.apps[i].manager = undefined;
                            this.apps[i].logonid = undefined;
                            this.apps[i].status = undefined;
                            this.apps[i].surname = undefined;
                            this.apps[i].initials = undefined;
                            this.apps[i].userlocation = undefined;
                            this.apps[i].comments = undefined; */

                        }
                        else {
                            //this.apps[i] = 0;
                            this.apps[i].application = "Total Counts";
                            this.apps[i].total = ttotal;
                            this.apps[i].expired = texpired;
                            this.apps[i].recertified = trecertified;
                            this.apps[i].pending = tpending;
                            this.apps[i].leavers = tleavers;
                            this.apps[i].removals = tremovals;
                            this.apps[i].noChange = tnoChange;
                            this.apps[i].movedToSpecial = tmovedToSpecial;
                        /*    this.apps[i]._id = undefined;
                            this.apps[i].manager = undefined;
                            this.apps[i].logonid = undefined;
                            this.apps[i].status = undefined;
                            this.apps[i].surname = undefined;
                            this.apps[i].initials = undefined;
                            this.apps[i].userlocation = undefined;
                            this.apps[i].comments = undefined; */
                        }


                        }
                  });







              }
        exportToExcel(event) {
            /*this.apps2=this.apps;
          for (let i in this.apps) {
            this.apps2[i].application = this.apps[i].application;
            this.apps2[i].total = this.apps[i].total;
            this.apps2[i].expired = this.apps[i].expired;
            this.apps2[i].recertified = this.apps[i].recertified;
            this.apps2[i].pending = this.apps[i].pending;
            this.apps2[i].leavers = this.apps[i].leavers;
            this.apps2[i].removals = this.apps[i].removals;
            this.apps2[i].noChange = this.apps[i].noChange;
            this.apps2[i].movedToSpecial = this.apps[i].movedToSpecial;
            //this.apps[i]._id = undefined;
            this.apps[i].manager = undefined;
            this.apps[i].logonid = undefined;
            this.apps[i].status = undefined;
            this.apps[i].surname = undefined;
            this.apps[i].initials = undefined;
            this.apps[i].userlocation = undefined;
            this.apps[i].commments = undefined;
          } */
              this.excelService.exportAsExcelFile(this.apps, 'PrimaryDashboard');
        }

        /* exportToExcel()
        {


          const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          const EXCEL_EXTENSION = '.xlsx';

          const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.apps);
          const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
          const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });


          //var htmltable= document.getElementById('report');
          //var html = htmltable.innerHTML;
          var fileName = "PrimaryDashboard";

          const data: Blob = new Blob([excelBuffer], {
            type: EXCEL_TYPE
          });

    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
          //window.open('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' + encodeURIComponent(html));
          //window.open('data:text/csv;charset=utf-8;' + encodeURIComponent(html));
   //text/csv;charset=utf-8;
           //var blob = new Blob([document.getElementById('report').innerHTML], {
           //  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8" });
           //  saveAs(blob, "report.xls");
          console.log("download button clicked");


      }*/
  }
