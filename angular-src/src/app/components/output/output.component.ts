import { Component, OnInit, Directive, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';
import {AuthService} from '../../services/auth.service';

import {ManagerExtract} from '../../models/managerextract';
import {ManagerextractService} from '../../services/managerextract.service';
import {SearchFilterPipe} from '../../services/searchfilter';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import {SelectModule} from 'angular2-select';

import {Product} from '../../models/product';
import {Country} from '../../models/country';

import { ExcelService } from '../../services/excel.service';



@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  //primarymanagers:ManagerExtract;
  primarymanagers:any[];
  statusOptions: any[] = [{ status: 'Not Recertified' }, { status: 'No Change' }, { status: 'Leaver' }, { status: 'Removal' }];
  userFilter: any = ({ manager: ''} || {resource:''} || {initials:''} || {userlocation:''} || {application:''} || {status:''} );
Games = [{name:"Not Recertified",type:"Not Recertified"},
{name:"No Change",type:"No Change"},
{name:"Leavers",type:"Leavers"},
{name:"Removals",type:"Removals"}];
statusselected: any;

//filter: ManagerExtract = new ManagerExtract();

public statusAll = [
    { value: 'NC', display: 'No Change' },
    { value: 'R', display: 'Removal' },
    { value: 'L', display: 'Leaver' },
    { value: 'NA', display: 'No Longer the Manager for User' }
];

public products: Product[] = [
      { "id": 1, "name": "Not Recertified" },
      { "id": 2, "name": "No Change" },
      { "id": 3, "name": "Leaver" },
      { "id": 4, "name": "Removal" },
    //  { "id": 5, "name": "Moved to Special Cycle" },
      { "id": 6, "name": "No Longer the Manager for User" }
    ];
    public selectedProduct: Product = this.products[0];
    onSelect() {
        this.selectedProduct = null;
        //console.log(this.products.name);
        //for (var i = 0; i < this.products.length; i++)
        //{
          //if (this.products[i].id == productId) {
          //  this.selectedProduct = this.products[i];
          //}
        //}
    }

   user: any;

  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         public route:ActivatedRoute,
         private excelService: ExcelService,
         private authService:AuthService,
         private router: Router) {
          this.getManagerExtracts();
          //this.getManagerExtractsClients();
          this.excelService = excelService;

          // var url = document.location.href;

          // var temp = url.split('?');

          //temp = temp.split('='); // Now we have all the values in an array, with each element looking like "var=val";

          // for(var i=0; i<temp.length; i++)
          // {
          //   var qval2 = temp[i].split("=");
          //   var qval = qval2[i];

          //   qval = qval.replace("%40", "@");
          //     // console.log(qval);
          //      this.user = qval;
          //   //document.getElementById(qval[0]).value = qval[1];
          // }
          //console.log("this.user::"+this.user);
        //  this.games = [{"name":"Not Recertified","type":"Not Recertified"},
        //  {"name":"No Change","type":"No Change"},
        //  {"name":"Leavers","type":"Leavers"},
        //  {"name":"Removals","type":"Removals"}];
        this.user = this.authService.getUser();
       }

  ngOnInit() {
    this.getManagerExtracts();

     
}
idList = [];
setToRentControl(value){
  var isList = [];
  this.idList = [];
  $("#filter_table_output tbody tr").each(function () {

   // isList.push(this.id);
 //  alert(this.name);
 //alert( $.trim($(this).find('td:eq(0)').html()));
 var managerId = $.trim($(this).find('td:eq(0)').html());
       // isList=[];
    if(value=='R'){
      $('#status-'+this.id).val('Removal');
    } else if(value=='NC'){
      $('#status-'+this.id).val('No Change');
    } else if(value=='L'){
      $('#status-'+this.id).val('Leaver');
    } else if(value=='NA'){
      $('#status-'+this.id).val('No Longer the Manager for User');
    }
    
    var data = {
      "id":managerId,
      "status":$('#status-'+this.id).val()
    };
    isList.push(data);
  });
 this.idList.push(isList);

 for (let i in this.primarymanagers) {
  for(let j in this.idList[0]){
     if(this.idList[0][j].id==this.primarymanagers[i]._id){
            this.primarymanagers[i].status = this.idList[0][j].status// here we are changing the status of the record   
        }
  }
}
    // if(value=='R')
    // {
      
    //   for (let i in this.primarymanagers) {
    //     this.primarymanagers[i].status = 'Removal';
    //   }

    // }
    // else if(value=='NC')
    // {
      
    //   for (let i in this.primarymanagers) {
    //     this.primarymanagers[i].status = 'No Change';
    //   }
    // }
    // else if(value=='L')
    // {
     
    //   for (let i in this.primarymanagers) {
    //     this.primarymanagers[i].status = 'Leaver';
    //   }
    // }
    // else if(value=='NA')
    // {
    //   //alert("seletced Leavers");
    //   //this.products = [  { "id": 3, "name": "Leavers" }];
    //   for (let i in this.primarymanagers) {
    //     this.primarymanagers[i].status = 'No Longer the Manager for User';
    //   }
    // }
    // else
    // {
    //   //alert("seletced None");
    //   this.products =  [
    //         { "id": 1, "name": "Not Recertified" },
    //         { "id": 2, "name": "No Change" },
    //         { "id": 3, "name": "Leaver" },
    //         { "id": 4, "name": "Removal" },
    //         //{ "id": 5, "name": "Moved to Special Cycle" },
    //         { "id": 5, "name": "No Longer the Manager for User" }
    //       ];
    // }
}


showSelectValue(mySelect) {
    console.log(mySelect);
}

clearSearch()
{
  window.location.reload();
}


  id = this.route.snapshot.params['id'];
  //this.selectedProduct = this.route.snapshot.params['id'];
  //primarymanagersList : ({});
  saveResponse(){
    console.log("in save response");
    console.log(this.primarymanagers);

    if(confirm("Are you sure want to submit responses ")) {
          console.log("Implement delete functionality here");

    //this.managerextractService.updateResponses(this.id,this.primarymanagers)
      //     .subscribe(()=> this.goBack());

    for (let i in this.primarymanagers) {
      for(let j in this.idList[0]){
        if(this.idList[0][j].id==this.primarymanagers[i]._id){
    var update = {
       _id: this.primarymanagers[i]._id,
       manager: this.primarymanagers[i].manager,
       resource : this.primarymanagers[i].resource,
       initials : this.primarymanagers[i].initials,
       logonid: this.primarymanagers[i].logonid,
       userlocation: this.primarymanagers[i].userlocation,
       application: this.primarymanagers[i].application,
       status:   this.primarymanagers[i].status
      }
     

    }
  }
   //console.log("Length::"+i+"::"+update);
   if(update!=null && update!=undefined){
   this.managerextractService.updateResponses(update._id,update)
   .subscribe(()=> this.goBack());
}
}
    this.flashMessage.show('Status submitted successfully', {cssClass: 'alert-success', timeout: 1000});
           //this.router.navigate(['/output']);
            //window.location.reload();
}

  }

goBack(){
(<HTMLInputElement>document.getElementById('submit')).disabled=true;
(<HTMLInputElement>document.getElementById('clear')).disabled=true;
(<HTMLInputElement>document.getElementById('export')).disabled=true;
window.setTimeout(this.refreshPage,5000);
}

refreshPage()
{
location.reload(true);
}


  //goBack(){
    //  window.location.reload();
  //this.router.navigate(['output'])

         //this.router.navigate(['/output']);
          //window.location.reload();
  //}





  getManagerExtracts(){
     console.log("this.user:getManagerExtracts:"+this.user);
     this.managerextractService.getManagerExtracts(this.user)
         .subscribe(primarymanagers=>{
           this.primarymanagers = primarymanagers;
       });

      /*this.managerextractService.getManagerExtracts()
          .subscribe(primarymanagers=>{
            this.primarymanagers = primarymanagers;
        }); */
    }

    /*getManagerExtractsClients(){
       console.log("this.user:getManagerExtractsClients:"+this.user);
        this.managerextractService.getManagerExtractsClients(this.user)
            .subscribe(primarymanagers=>{
              this.primarymanagers = primarymanagers;
          });
      }*/

    public items:Array<string> = ['Not Recertified','Leavers','Removals','No Change'];

    private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;

  private get disabledV():string {
    return this._disabledV;
  }

  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }

  public selected(value:any):void {
    console.log('Selected value is: ', value);
  }

  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }

  public typed(value:any):void {
    console.log('New search input: ', value);
  }

  public refreshValue(value:any):void {
    this.value = value;
  }

  exportToExcel(event) {

    var json=[];

    for(let i in this.primarymanagers){
      var update = {
        Manager: this.primarymanagers[i].manager,
        Resource : this.primarymanagers[i].resource,
        Logon_Id: this.primarymanagers[i].logonid,
        Region: this.primarymanagers[i].userlocation,
        Application: this.primarymanagers[i].application,
        Status:   this.primarymanagers[i].status
       }

       json.push(update);
    }

    this.excelService.exportAsExcelFile(json, 'Primary_Cycle');
  }

}
