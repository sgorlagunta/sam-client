import { Component, OnInit } from '@angular/core';
import {PrimaryapplicationsService} from '../../services/primaryapplications.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Primaryapplications } from './primaryapplications'


@Component({
  selector: 'app-primaryapplicationsadd',
  templateUrl: './primaryapplicationsadd.component.html',
  styleUrls: ['./primaryapplicationsadd.component.css']
})
export class PrimaryapplicationsaddComponent implements OnInit {



  constructor(
    public primaryapplicationsService:PrimaryapplicationsService,
    public route:ActivatedRoute,
    public router:Router
  ) { }

  ngOnInit() {
  }

primaryapplications = new Primaryapplications();
  addPrimaryApplications(){
    this.primaryapplicationsService.addPrimaryApplications(this.primaryapplications)
        .subscribe(()=> this.goBack())
  }
  goBack(){
  this.router.navigate(['/primaryapplications'])
}
}
