import { Component, OnInit } from '@angular/core';
import {MloginService} from '../../services/mlogin.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-mlogin',
  templateUrl: './mlogin.component.html',
  styleUrls: ['./mlogin.component.css']
})
export class MloginComponent implements OnInit {

  constructor(private mloginService:MloginService,
  private router:Router,
  public route:ActivatedRoute,
  private flashMessage:FlashMessagesService
) { }

  ngOnInit() {
  }

  email = this.route.snapshot.params['email'];

  onLoginSubmit(){
      //this.mloginService.authenticateManager(this.email).subscribe(data => {
      //  console.log('client',+this.email);
      //if(data.success){
        //this.mloginService.storeUserData(data.token, data.user);
        this.flashMessage.show('You are now logged in', {
          cssClass: 'alert-success',
          timeout: 5000});
      //  this.router.navigate(['dashboard2']);
      //} else {
      //  this.flashMessage.show(data.msg, {
      //    cssClass: 'alert-danger',
      //    timeout: 5000});
          this.router.navigate(['mdashboard']);
      //}
    //});
}
}
