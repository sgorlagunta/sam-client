import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Headers, Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ApiService {

  constructor(private http: Http){
   console.log('PostService Initialized...');
 }

 getApplications(){
   return this.http.get("http://localhost:8085/api/upload")
       .map(res => res.json());
 }

 deleteApplication(id){
   return this.http.delete("http://localhost:8085/api/upload/"+id)
       .map(res => res.json());
 }

 getApplication(id){
   return this.http.get("http://localhost:8085/api/upload/"+id)
       .map(res => res.json());
 }

 updateApplication(id, info){
   return this.http.put("http://localhost:8085/api/upload/"+id,info)
       .map(res => res.json());
 }
 addApplication(info){
  return this.http.post("http://localhost:8085/api/uploadapplication",info)
      .map(res => res.json());
}
}
