import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MloginService {

  constructor(private http:Http) { }

  authenticateManager(info){
    return this.http.get("http://localhost:8085/api/mlogin",info)
        .map(res => res.json());
  }

}
