import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PrimaryapplicationsService {
  constructor( private http:Http) { }

  getPrimaryApplications(){
    return this.http.get("http://localhost:8085/api/primaryapplications")
        .map(res => res.json());
  }
   addPrimaryApplications(info){
    return this.http.post("http://localhost:8085/api/primaryapplications",info)
        .map(res => res.json());
  }
  getPrimaryApplication(id){
    return this.http.get("http://localhost:8085/api/primaryapplications/"+id)
        .map(res => res.json());
  }
  deletePrimaryApplications(id){
    return this.http.delete("http://localhost:8085/api/primaryapplications/"+id)
        .map(res => res.json());
  }
  updatePrimaryApplications(id, info){
    return this.http.put("http://localhost:8085/api/primaryapplications/"+id,info)
        .map(res => res.json());
  }

}
