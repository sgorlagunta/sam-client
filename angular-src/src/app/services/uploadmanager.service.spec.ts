import { TestBed, inject } from '@angular/core/testing';

import { UploadmanagerService } from './uploadmanager.service';

describe('UploadmanagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadmanagerService]
    });
  });

  it('should be created', inject([UploadmanagerService], (service: UploadmanagerService) => {
    expect(service).toBeTruthy();
  }));
});
