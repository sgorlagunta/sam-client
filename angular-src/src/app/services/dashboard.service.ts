import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map' ;

@Injectable()
export class DashboardService {

  constructor( private http:Http) { }
  getUsersDashboard(){
      return this.http.get("http://localhost:8085/api/dashboard")
          .map(res => res.json());
    }

}
