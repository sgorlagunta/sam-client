import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map' ;

@Injectable()
export class UserextractService {

  constructor( private http:Http) { }
  getUsers(){
    return this.http.get("http://localhost:8085/api/userextract")
        .map(res => res.json());
  }

}
