import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map' ;

@Injectable()
export class UploadmanagerService {

  constructor( private http:Http) { }
  getUploadmanagers(){
      return this.http.get("http://localhost:8085/api/uploadmanager")
          .map(res => res.json());
    }

}
