import { TestBed, inject } from '@angular/core/testing';

import { PrimaryapplicationsService } from './primaryapplications.service';

describe('PrimaryapplicationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrimaryapplicationsService]
    });
  });

  it('should be created', inject([PrimaryapplicationsService], (service: PrimaryapplicationsService) => {
    expect(service).toBeTruthy();
  }));
});
