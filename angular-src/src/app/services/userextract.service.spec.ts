import { TestBed, inject } from '@angular/core/testing';

import { UserextractService } from './userextract.service';

describe('UserextractService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserextractService]
    });
  });

  it('should be created', inject([UserextractService], (service: UserextractService) => {
    expect(service).toBeTruthy();
  }));
});
