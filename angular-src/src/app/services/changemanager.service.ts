import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ChangemanagerService {

  constructor( private http:Http) { }

  getManagers(){
    return this.http.get("http://localhost:8085/api/changemanager")
        .map(res => res.json());
  }

  updateManager(id, info){
    return this.http.put("http://localhost:8085/api/changemanager/"+id,info)
        .map(res => res.json());
  }

}
