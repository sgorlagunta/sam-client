import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {DropdownModule} from "ng2-dropdown";

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';

import {ValidateService} from './services/validate.service';
import {AuthService} from './services/auth.service';
import {FlashMessagesModule} from 'angular2-flash-messages';
import {AuthGuard} from './guards/auth.guard';
import { PrimaryapplicationsComponent } from './components/primaryapplications/primaryapplications.component';
import { PrimaryapplicationsService} from './services/primaryapplications.service';
import { PrimaryapplicationsaddComponent } from './components/primaryapplicationsadd/primaryapplicationsadd.component';
import { PrimaryapplicationshowComponent } from './components/primaryapplicationshow/primaryapplicationshow.component';
import { PrimaryapplicationeditComponent } from './components/primaryapplicationedit/primaryapplicationedit.component';
import { UseruploadComponent } from './components/userupload/userupload.component';
import { UploaduserService} from './services/uploaduser.service';
import { FileUploadModule } from 'ng2-file-upload';
import { ManageruploadComponent } from './components/managerupload/managerupload.component';
import { UploadmanagerService} from './services/uploadmanager.service';
import { ChangemanagerComponent } from './components/changemanager/changemanager.component';
import { ChangemanagerService} from './services/changemanager.service';
import { ChangemanagereditComponent } from './components/changemanager/changemanageredit/changemanageredit.component';
import { MloginComponent } from './components/mlogin/mlogin.component';
import { MloginService } from './services/mlogin.service';
import { ManagerdashboardComponent } from './components/managerdashboard/managerdashboard.component';
import { ApplicationsComponent } from './components/applications/applications.component';
import { ApiService } from './services/api.service';
import { ApplicationseditComponent } from './components/applications/applicationsedit/applicationsedit.component';
import { ApplicationsshowComponent } from './components/applications/applicationsshow/applicationsshow.component';
import { ApplicationsaddComponent } from './components/applications/applicationsadd/applicationsadd.component';
import { Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
import { PagerService } from './services/index';
import { UserextractComponent } from './components/userextract/userextract.component';
import { UserextractService } from './services/userextract.service';
import { ManagerexxtractComponent } from './components/managerexxtract/managerexxtract.component';
import { ManagerextractService } from './services/managerextract.service';
import { ManagerchangeComponent } from './components/managerchange/managerchange.component';
import { ManagerchangeeditComponent } from './components/managerchange/managerchangeedit/managerchangeedit.component';
import { DashboardService } from './services/dashboard.service';



import { OutputComponent } from './components/output/output.component';
import { SearchFilterPipe } from './services/searchfilter';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import {SelectModule} from 'angular2-select';
import { Ng2TableModule } from 'ng2-table/ng2-table';

import { ExcelService } from './services/excel.service';
import { DelegatemanagerComponent } from './components/delegatemanager/delegatemanager.component';





const appRoutes: Routes =  [
  {path:'', component: HomeComponent},
  {path:'register', component: RegisterComponent},
  {path:'login', component: LoginComponent},
  {path:'dashboard', component: DashboardComponent, canActivate:[AuthGuard]},
  {path:'profile', component: ProfileComponent, canActivate:[AuthGuard]},
  {path:'primaryapplications', component: PrimaryapplicationsComponent, canActivate:[AuthGuard]},
  {path:'addprimaryapplications', component: PrimaryapplicationsaddComponent, canActivate:[AuthGuard]},
  {path:'showprimaryapplications/:id', component: PrimaryapplicationshowComponent, canActivate:[AuthGuard]},
  {path:'editprimaryapplications/:id', component: PrimaryapplicationeditComponent, canActivate:[AuthGuard]},
  {path:'uploaduser', component: UseruploadComponent, canActivate:[AuthGuard]},
  {path:'uploadmanager', component: ManageruploadComponent, canActivate:[AuthGuard]},
  {path:'changemanager', component: ChangemanagerComponent, canActivate:[AuthGuard]},
  {path:'editchangemanager', component: ChangemanagereditComponent, canActivate:[AuthGuard]},
  {path:'mlogin', component: MloginComponent, canActivate:[AuthGuard]},
  {path:'mdashboard', component: ManagerdashboardComponent, canActivate:[AuthGuard]},
  {path:'applications', component: ApplicationsComponent, canActivate:[AuthGuard] },
  {path:'editapplication/:id', component: ApplicationseditComponent, canActivate:[AuthGuard]},
  {path:'showapplication/:id', component: ApplicationsshowComponent, canActivate:[AuthGuard]},
  {path:'addapplications', component: ApplicationsaddComponent, canActivate:[AuthGuard]},
  {path:'userextracts', component: UserextractComponent, canActivate:[AuthGuard]},
  {path:'managerextracts', component: ManagerexxtractComponent, canActivate:[AuthGuard]},
  {path:'managerchange', component: ManagerchangeComponent, canActivate:[AuthGuard]},
  {path:'managerchangeedit/:id', component: ManagerchangeeditComponent, canActivate:[AuthGuard]},
  {path:'output', component: OutputComponent, canActivate:[AuthGuard]},
  {path:'delegate', component: DelegatemanagerComponent, canActivate:[AuthGuard]}
  ]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    PrimaryapplicationsComponent,
    PrimaryapplicationsaddComponent,
    PrimaryapplicationshowComponent,
    PrimaryapplicationeditComponent,
    UseruploadComponent,
    ManageruploadComponent,
    ChangemanagerComponent,
    ChangemanagereditComponent,
    MloginComponent,
    ManagerdashboardComponent,
    ApplicationsComponent,
    ApplicationseditComponent,
    ApplicationsshowComponent,
    ApplicationsaddComponent,
    UserextractComponent,
    ManagerexxtractComponent,
    ManagerchangeComponent,
    ManagerchangeeditComponent,
    OutputComponent,
    SearchFilterPipe,
    DelegatemanagerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule,
    DropdownModule,
    FileUploadModule,
    Ng2PaginationModule,
    Ng2SearchPipeModule,
    Ng2FilterPipeModule,
    SelectModule,
    Ng2TableModule
  ],
  providers: [ValidateService, AuthService, AuthGuard,PrimaryapplicationsService,
    UploaduserService,UploadmanagerService,ChangemanagerService,MloginService,ApiService,PagerService,
    UserextractService,ManagerextractService,DashboardService,ExcelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
