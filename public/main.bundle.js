webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "a {\r\n    cursor: pointer;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"container\">\n  <flash-messages></flash-messages>\n  <router-outlet></router-outlet>  \n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app works!';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dropdown__ = __webpack_require__("../../../../ng2-dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dropdown___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_dropdown__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_profile_profile_component__ = __webpack_require__("../../../../../src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_primaryapplications_primaryapplications_component__ = __webpack_require__("../../../../../src/app/components/primaryapplications/primaryapplications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_primaryapplications_service__ = __webpack_require__("../../../../../src/app/services/primaryapplications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_primaryapplicationsadd_primaryapplicationsadd_component__ = __webpack_require__("../../../../../src/app/components/primaryapplicationsadd/primaryapplicationsadd.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_primaryapplicationshow_primaryapplicationshow_component__ = __webpack_require__("../../../../../src/app/components/primaryapplicationshow/primaryapplicationshow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_primaryapplicationedit_primaryapplicationedit_component__ = __webpack_require__("../../../../../src/app/components/primaryapplicationedit/primaryapplicationedit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_userupload_userupload_component__ = __webpack_require__("../../../../../src/app/components/userupload/userupload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_uploaduser_service__ = __webpack_require__("../../../../../src/app/services/uploaduser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_managerupload_managerupload_component__ = __webpack_require__("../../../../../src/app/components/managerupload/managerupload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_uploadmanager_service__ = __webpack_require__("../../../../../src/app/services/uploadmanager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_changemanager_changemanager_component__ = __webpack_require__("../../../../../src/app/components/changemanager/changemanager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_changemanager_service__ = __webpack_require__("../../../../../src/app/services/changemanager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__components_changemanager_changemanageredit_changemanageredit_component__ = __webpack_require__("../../../../../src/app/components/changemanager/changemanageredit/changemanageredit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_mlogin_mlogin_component__ = __webpack_require__("../../../../../src/app/components/mlogin/mlogin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__services_mlogin_service__ = __webpack_require__("../../../../../src/app/services/mlogin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__components_managerdashboard_managerdashboard_component__ = __webpack_require__("../../../../../src/app/components/managerdashboard/managerdashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__components_applications_applications_component__ = __webpack_require__("../../../../../src/app/components/applications/applications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__components_applications_applicationsedit_applicationsedit_component__ = __webpack_require__("../../../../../src/app/components/applications/applicationsedit/applicationsedit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__components_applications_applicationsshow_applicationsshow_component__ = __webpack_require__("../../../../../src/app/components/applications/applicationsshow/applicationsshow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__components_applications_applicationsadd_applicationsadd_component__ = __webpack_require__("../../../../../src/app/components/applications/applicationsadd/applicationsadd.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38_ng2_pagination__ = __webpack_require__("../../../../ng2-pagination/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38_ng2_pagination___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_38_ng2_pagination__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__services_index__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_userextract_userextract_component__ = __webpack_require__("../../../../../src/app/components/userextract/userextract.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__services_userextract_service__ = __webpack_require__("../../../../../src/app/services/userextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__components_managerexxtract_managerexxtract_component__ = __webpack_require__("../../../../../src/app/components/managerexxtract/managerexxtract.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__services_managerextract_service__ = __webpack_require__("../../../../../src/app/services/managerextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__components_managerchange_managerchange_component__ = __webpack_require__("../../../../../src/app/components/managerchange/managerchange.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__components_managerchange_managerchangeedit_managerchangeedit_component__ = __webpack_require__("../../../../../src/app/components/managerchange/managerchangeedit/managerchangeedit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__services_dashboard_service__ = __webpack_require__("../../../../../src/app/services/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__components_output_output_component__ = __webpack_require__("../../../../../src/app/components/output/output.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__services_searchfilter__ = __webpack_require__("../../../../../src/app/services/searchfilter.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49_ng2_search_filter__ = __webpack_require__("../../../../ng2-search-filter/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49_ng2_search_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_49_ng2_search_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50_ng2_filter_pipe__ = __webpack_require__("../../../../ng2-filter-pipe/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50_ng2_filter_pipe___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_50_ng2_filter_pipe__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51_angular2_select__ = __webpack_require__("../../../../angular2-select/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51_angular2_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_51_angular2_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52_ng2_table_ng2_table__ = __webpack_require__("../../../../ng2-table/ng2-table.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52_ng2_table_ng2_table___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_52_ng2_table_ng2_table__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__components_delegatemanager_delegatemanager_component__ = __webpack_require__("../../../../../src/app/components/delegatemanager/delegatemanager.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






































 //importing ng2-pagination
















var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_10__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_9__components_register_register_component__["a" /* RegisterComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_8__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_11__components_dashboard_dashboard_component__["a" /* DashboardComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_12__components_profile_profile_component__["a" /* ProfileComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'primaryapplications', component: __WEBPACK_IMPORTED_MODULE_17__components_primaryapplications_primaryapplications_component__["a" /* PrimaryapplicationsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'addprimaryapplications', component: __WEBPACK_IMPORTED_MODULE_19__components_primaryapplicationsadd_primaryapplicationsadd_component__["a" /* PrimaryapplicationsaddComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'showprimaryapplications/:id', component: __WEBPACK_IMPORTED_MODULE_20__components_primaryapplicationshow_primaryapplicationshow_component__["a" /* PrimaryapplicationshowComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'editprimaryapplications/:id', component: __WEBPACK_IMPORTED_MODULE_21__components_primaryapplicationedit_primaryapplicationedit_component__["a" /* PrimaryapplicationeditComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'uploaduser', component: __WEBPACK_IMPORTED_MODULE_22__components_userupload_userupload_component__["a" /* UseruploadComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'uploadmanager', component: __WEBPACK_IMPORTED_MODULE_25__components_managerupload_managerupload_component__["a" /* ManageruploadComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'changemanager', component: __WEBPACK_IMPORTED_MODULE_27__components_changemanager_changemanager_component__["a" /* ChangemanagerComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'editchangemanager', component: __WEBPACK_IMPORTED_MODULE_29__components_changemanager_changemanageredit_changemanageredit_component__["a" /* ChangemanagereditComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'mlogin', component: __WEBPACK_IMPORTED_MODULE_30__components_mlogin_mlogin_component__["a" /* MloginComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'mdashboard', component: __WEBPACK_IMPORTED_MODULE_32__components_managerdashboard_managerdashboard_component__["a" /* ManagerdashboardComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'applications', component: __WEBPACK_IMPORTED_MODULE_33__components_applications_applications_component__["a" /* ApplicationsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'editapplication/:id', component: __WEBPACK_IMPORTED_MODULE_35__components_applications_applicationsedit_applicationsedit_component__["a" /* ApplicationseditComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'showapplication/:id', component: __WEBPACK_IMPORTED_MODULE_36__components_applications_applicationsshow_applicationsshow_component__["a" /* ApplicationsshowComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'addapplications', component: __WEBPACK_IMPORTED_MODULE_37__components_applications_applicationsadd_applicationsadd_component__["a" /* ApplicationsaddComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'userextracts', component: __WEBPACK_IMPORTED_MODULE_40__components_userextract_userextract_component__["a" /* UserextractComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'managerextracts', component: __WEBPACK_IMPORTED_MODULE_42__components_managerexxtract_managerexxtract_component__["a" /* ManagerexxtractComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'managerchange', component: __WEBPACK_IMPORTED_MODULE_44__components_managerchange_managerchange_component__["a" /* ManagerchangeComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'managerchangeedit/:id', component: __WEBPACK_IMPORTED_MODULE_45__components_managerchange_managerchangeedit_managerchangeedit_component__["a" /* ManagerchangeeditComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'output', component: __WEBPACK_IMPORTED_MODULE_47__components_output_output_component__["a" /* OutputComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'delegate', component: __WEBPACK_IMPORTED_MODULE_54__components_delegatemanager_delegatemanager_component__["a" /* DelegatemanagerComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */]] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_primaryapplications_primaryapplications_component__["a" /* PrimaryapplicationsComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_primaryapplicationsadd_primaryapplicationsadd_component__["a" /* PrimaryapplicationsaddComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_primaryapplicationshow_primaryapplicationshow_component__["a" /* PrimaryapplicationshowComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_primaryapplicationedit_primaryapplicationedit_component__["a" /* PrimaryapplicationeditComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_userupload_userupload_component__["a" /* UseruploadComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_managerupload_managerupload_component__["a" /* ManageruploadComponent */],
                __WEBPACK_IMPORTED_MODULE_27__components_changemanager_changemanager_component__["a" /* ChangemanagerComponent */],
                __WEBPACK_IMPORTED_MODULE_29__components_changemanager_changemanageredit_changemanageredit_component__["a" /* ChangemanagereditComponent */],
                __WEBPACK_IMPORTED_MODULE_30__components_mlogin_mlogin_component__["a" /* MloginComponent */],
                __WEBPACK_IMPORTED_MODULE_32__components_managerdashboard_managerdashboard_component__["a" /* ManagerdashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_33__components_applications_applications_component__["a" /* ApplicationsComponent */],
                __WEBPACK_IMPORTED_MODULE_35__components_applications_applicationsedit_applicationsedit_component__["a" /* ApplicationseditComponent */],
                __WEBPACK_IMPORTED_MODULE_36__components_applications_applicationsshow_applicationsshow_component__["a" /* ApplicationsshowComponent */],
                __WEBPACK_IMPORTED_MODULE_37__components_applications_applicationsadd_applicationsadd_component__["a" /* ApplicationsaddComponent */],
                __WEBPACK_IMPORTED_MODULE_40__components_userextract_userextract_component__["a" /* UserextractComponent */],
                __WEBPACK_IMPORTED_MODULE_42__components_managerexxtract_managerexxtract_component__["a" /* ManagerexxtractComponent */],
                __WEBPACK_IMPORTED_MODULE_44__components_managerchange_managerchange_component__["a" /* ManagerchangeComponent */],
                __WEBPACK_IMPORTED_MODULE_45__components_managerchange_managerchangeedit_managerchangeedit_component__["a" /* ManagerchangeeditComponent */],
                __WEBPACK_IMPORTED_MODULE_47__components_output_output_component__["a" /* OutputComponent */],
                __WEBPACK_IMPORTED_MODULE_48__services_searchfilter__["a" /* SearchFilterPipe */],
                __WEBPACK_IMPORTED_MODULE_54__components_delegatemanager_delegatemanager_component__["a" /* DelegatemanagerComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_15_angular2_flash_messages__["FlashMessagesModule"],
                __WEBPACK_IMPORTED_MODULE_5_ng2_dropdown__["DropdownModule"],
                __WEBPACK_IMPORTED_MODULE_24_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_38_ng2_pagination__["Ng2PaginationModule"],
                __WEBPACK_IMPORTED_MODULE_49_ng2_search_filter__["Ng2SearchPipeModule"],
                __WEBPACK_IMPORTED_MODULE_50_ng2_filter_pipe__["Ng2FilterPipeModule"],
                __WEBPACK_IMPORTED_MODULE_51_angular2_select__["SelectModule"],
                __WEBPACK_IMPORTED_MODULE_52_ng2_table_ng2_table__["Ng2TableModule"]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_13__services_validate_service__["a" /* ValidateService */], __WEBPACK_IMPORTED_MODULE_14__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_16__guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_18__services_primaryapplications_service__["a" /* PrimaryapplicationsService */],
                __WEBPACK_IMPORTED_MODULE_23__services_uploaduser_service__["a" /* UploaduserService */], __WEBPACK_IMPORTED_MODULE_26__services_uploadmanager_service__["a" /* UploadmanagerService */], __WEBPACK_IMPORTED_MODULE_28__services_changemanager_service__["a" /* ChangemanagerService */], __WEBPACK_IMPORTED_MODULE_31__services_mlogin_service__["a" /* MloginService */], __WEBPACK_IMPORTED_MODULE_34__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_39__services_index__["a" /* PagerService */],
                __WEBPACK_IMPORTED_MODULE_41__services_userextract_service__["a" /* UserextractService */], __WEBPACK_IMPORTED_MODULE_43__services_managerextract_service__["a" /* ManagerextractService */], __WEBPACK_IMPORTED_MODULE_46__services_dashboard_service__["a" /* DashboardService */], __WEBPACK_IMPORTED_MODULE_53__services_excel_service__["a" /* ExcelService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/app.module.js.map

/***/ }),

/***/ "../../../../../src/app/components/applications/applications.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div.centre {\r\n  width: 500px;\r\n  height: auto;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/applications/applications.component.html":
/***/ (function(module, exports) {

module.exports = "<h4>\r\n<!-- here we echo the title from the component -->\r\n  <b>{{title}}</b>\r\n</h4>\r\n\r\n<!-- File input for the file-upload plugin, with special ng2-file-upload directive called ng2FileSelect -->\r\n<input type=\"file\" name=\"photo\" ng2FileSelect [uploader]=\"uploader\" />\r\n<!-- button to trigger the file upload when submitted -->\r\n<button type=\"button\" class=\"btn btn-success btn-s\" (click)=\"uploader.uploadAll()\" [disabled]=\"!uploader.getNotUploadedItems().length\">\r\n      <span class=\"glyphicon glyphicon-upload\"></span>Upload Application\r\n</button>\r\n\r\n\r\n<a class=\"btn btn-info\" routerLink=\"/addapplications\">Manual Entry Application</a>\r\n<button (click)=\"exportToExcel()\" class=\"btn btn-primary\">Export to excel</button>\r\n<br />\r\n<p>Total Applications: <b>{{ primaryapplications ? primaryapplications.length: '0' }}</b></p>\r\n\r\n <!-- <div class=\"navbar-nav navbar-right\">\r\n   <a class=\"btn btn-light\"  (click)=\"download()\">Download Report</a>\r\n </div> -->\r\n\r\n<table class=\"table table-bordered table-striped\" id=\"my-table-id\">\r\n\t\t  <thead>\r\n\t\t\t<tr>\r\n\t\t\t  <th>Application Name</th>\r\n\t\t\t  <th>Application Owner</th>\r\n\t\t\t  <th>Business Unit</th>\r\n        <th>Platform</th>\r\n\t\t\t  <th width=\"270\" align=\"center\">Action</th>\r\n\t\t\t</tr>\r\n\t\t  </thead>\r\n\t\t  <tbody>\r\n\t\t\t <tr  *ngFor=\"let primaryapplication of primaryapplications | paginate: {itemsPerPage: 5, currentPage:page, id: '1'}; let i = index\">\r\n\t\t\t\t<td>{{primaryapplication.applicationName}}</td>\r\n\t\t\t\t<td>{{primaryapplication.applicationOwner}}</td>\r\n\t\t\t\t<td>{{primaryapplication.bu}}</td>\r\n        <td>{{primaryapplication.platform}}</td>\r\n\t\t\t\t<td width=\"270\">\r\n\t\t\t\t\t<a class=\"btn btn-info\" routerLink=\"/showapplication/{{primaryapplication._id}}\">Details</a>\r\n\t\t\t\t\t<a class=\"btn btn-success\"  routerLink=\"/editapplication/{{primaryapplication._id}}\">Edit</a>\r\n\t\t\t\t\t<a class=\"btn btn-danger\"  (click)=\"deleteApplication(primaryapplication._id)\">Delete</a>\r\n        </td>\r\n\t\t\t </tr>\r\n\t\t  </tbody>\r\n\t\t</table>\r\n    <div class=\"centre\">\r\n\r\n\r\n                <pagination-controls  id=\"1\"\r\n                      (pageChange)=\"page = $event\"\r\n                      maxSize=\"7\"\r\n                      directionLinks=\"true\"\r\n                      autoHide=\"true\"\r\n                      previousLabel=\"Previous\"\r\n                      nextLabel=\"Next\"\r\n                      screenReaderPaginationLabel=\"Pagination\"\r\n                      screenReaderPageLabel=\"page\"\r\n                      screenReaderCurrentLabel=\"You're on page\">\r\n</pagination-controls>\r\n   </div>\r\n\r\n      <!-- <div id=\"report\" hidden>\r\n        <table class=\"table table-bordered table-striped\" >\r\n        \t\t  <thead>\r\n        \t\t\t<tr>\r\n        \t\t\t  <th>Application Name</th>\r\n        \t\t\t  <th>Application Owner</th>\r\n        \t\t\t  <th>Business Unit</th>\r\n                <th>Platform</th>\r\n                <th>Classification</th>\r\n                <th>Scope</th>\r\n                <th>SAM Contact Person</th>\r\n                <th>Comments</th>\r\n        \t\t\t</tr>\r\n        \t\t  </thead>\r\n        \t\t  <tbody>\r\n        \t\t\t <tr  *ngFor=\"let primaryapplication of primaryapplications\">\r\n        \t\t\t\t<td>{{primaryapplication.applicationName}}</td>\r\n        \t\t\t\t<td>{{primaryapplication.applicationOwner}}</td>\r\n        \t\t\t\t<td>{{primaryapplication.bu}}</td>\r\n                <td>{{primaryapplication.platform}}</td>\r\n                <td>{{primaryapplication.classification}}</td>\r\n                <td>{{primaryapplication.scope}}</td>\r\n                <td>{{primaryapplication.poc}}</td>\r\n                <td>{{primaryapplication.comments}}</td>\r\n        \t\t\t </tr>\r\n        \t\t  </tbody>\r\n        \t\t</table>\r\n        </div> -->\r\n"

/***/ }),

/***/ "../../../../../src/app/components/applications/applications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import component, ElementRef, input and the oninit method from angular core

//import the file-upload plugin

//import the native angular http and respone libraries

//import the do function to be used with the http library.

//import the map function to be used with the http library



var URL = 'http://localhost:8080/api/upload';
var ApplicationsComponent = /** @class */ (function () {
    //declare a constroctur, so we can pass in some properties to the class, which can be    //accessed using the this variable
    function ApplicationsComponent(http, el, flashMessage, apiService, excelService, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.apiService = apiService;
        this.excelService = excelService;
        this.router = router;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: URL, itemAlias: 'photo' });
        //This is the default title property created by the angular cli. Its responsible for the app works
        this.title = 'Applications In Scope for Recertification';
        this.getApplications();
        this.excelService = excelService;
    }
    ApplicationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
        this.uploader.onAfterAddingFile = function (file) { file.withCredentials = false; };
        //overide the onCompleteItem property of the uploader so we are
        //able to deal with the server response.
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            console.log("ImageUpload:uploaded:", item, status, response);
            var responsePath = JSON.parse(response);
            console.log("ImageUpload--> completion:", response, responsePath); // the url will be in the response
            _this.flashMessage.show('Files uploaded successfully', { cssClass: 'alert-success', timeout: 3000 });
            //this.router.navigate(['/applications']);
            window.location.reload();
        };
        /*this.uploader.onCompleteAll = function() {
           console.info('onCompleteAll');
           window.location.reload();
         }; */
    };
    //the function which handles the file upload without using a plugin.
    ApplicationsComponent.prototype.upload = function () {
        //locate the file element meant for the file upload.
        var inputEl = this.el.nativeElement.querySelector('#photo');
        //get the total amount of files attached to the file input.
        var fileCount = inputEl.files.length;
        //create a new fromdata instance
        var formData = new FormData();
        //check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) {
            //append the key name 'photo' with the first file in the element
            formData.append('photo', inputEl.files.item(0));
            //call the angular http method
            this.http
                .post(URL, formData).map(function (res) { return res.json(); }).subscribe(
            //map the success function and alert the response
            function (success) {
                alert(success._body);
            }, function (error) { return alert(error); });
        }
    };
    ApplicationsComponent.prototype.getApplications = function () {
        var _this = this;
        this.apiService.getApplications()
            .subscribe(function (primaryapplications) {
            _this.primaryapplications = primaryapplications;
            /*  this.primary = this.primaryapplications;



              for (var key in this.primary){
                //console.log( "this.apps: keys"+key, this.apps[key] );
                delete this.primary[key]._id;
                //console.log( "this.apps: keys"+key, this.apps[key] );
              } */
        });
    };
    ApplicationsComponent.prototype.deleteApplication = function (id) {
        var _this = this;
        if (confirm("Are you sure to delete application ")) {
            console.log("Implement delete functionality here");
            this.apiService.deleteApplication(id)
                .subscribe(function () {
                _this.getApplications();
            });
        }
    };
    ApplicationsComponent.prototype.getDeletion = function (id) {
        var _this = this;
        this.apiService.deleteApplication(id)
            .subscribe(function () {
            _this.getApplications();
        });
    };
    ApplicationsComponent.prototype.download = function () {
        var htmltable = document.getElementById('report');
        var html = htmltable.innerHTML;
        window.open('data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html));
        //window.open('data:text/csv;charset=utf-8;' + encodeURIComponent(html));
        //text/csv;charset=utf-8;
        //var blob = new Blob([document.getElementById('report').innerHTML], {
        //  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8" });
        //  saveAs(blob, "report.xls");
        console.log("download button clicked");
    };
    ApplicationsComponent.prototype.exportToExcel = function (event) {
        this.excelService.exportAsExcelFile(this.primaryapplications, 'primaryapplications');
    };
    ApplicationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-applications',
            template: __webpack_require__("../../../../../src/app/components/applications/applications.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/applications/applications.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_8__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__services_excel_service__["a" /* ExcelService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _f || Object])
    ], ApplicationsComponent);
    return ApplicationsComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/applications.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsadd/applicationsadd.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsadd/applicationsadd.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Add Application Entry Form </div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\" (submit)=\"addApplication()\">\n<div class=\"form-group\">\n  <label for=\"name\" class=\"col-sm-2 control-label\">Application Name : </label>\n  <div class=\"col-sm-3\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationName\" name=\"applicationName\" >\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"owner\" class=\"col-sm-2 control-label\">Application Owner : </label>\n  <div class=\"col-sm-3\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationOwner\" name=\"applicationOwner\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"bu\" class=\"col-sm-2 control-label\">Business Unit : </label>\n<div class=\"col-sm-2\">\n  <select [(ngModel)]=\"primaryapplications.bu\" name =\"primaryapplications.bu\" class=\"form-control\" style=\"height: 30px\">\n      <option  *ngFor=\"let product of bu\"\n        [value]=\"product.name\"\n      >\n      {{product.name}}\n      </option>\n    </select>\n  </div>\n\n\n</div>\n<div class=\"form-group\">\n  <label for=\"platform\" class=\"col-sm-2 control-label\">Platform : </label>\n  <div class=\"col-sm-2\">\n    <select [(ngModel)]=\"primaryapplications.platform\" name =\"primaryapplications.platform\" class=\"form-control\" style=\"height: 30px\">\n        <option  *ngFor=\"let product of platform\"\n          [value]=\"product.name\"\n        >\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"classification\" class=\"col-sm-2 control-label\">Classification : </label>\n  <div class=\"col-sm-3\">\n    <select [(ngModel)]=\"primaryapplications.classification\" name =\"primaryapplications.classification\" class=\"form-control\" style=\"height: 35px\">\n        <option  *ngFor=\"let product of classification\"\n          [value]=\"product.name\"\n        >\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"scope\" class=\"col-sm-2 control-label\">Scope : </label>\n  <div class=\"col-sm-3\">\n    <select [(ngModel)]=\"primaryapplications.scope\" name =\"primaryapplications.scope\" class=\"form-control\" style=\"height: 35px\">\n        <option  *ngFor=\"let product of scope\"\n          [value]=\"product.name\"\n        >\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"poc\" class=\"col-sm-2 control-label\">SAM Contact Person : </label>\n  <div class=\"col-sm-4\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.poc\" name=\"poc\">\n  </div>\n</div>\n\n<div class=\"form-group\">\n  <label for=\"comments\" class=\"col-sm-2 control-label\">Comments : </label>\n  <div class=\"col-sm-4\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.comments\" name=\"comments\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-2 col-sm-8\">\n    <button type=\"submit\" class=\"btn btn-success\">Save</button>\n     <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\n  </div>\n\n</div>\n</form>\n\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsadd/applicationsadd.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationsaddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_applications__ = __webpack_require__("../../../../../src/app/models/applications.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import component, ElementRef, input and the oninit method from angular core

//import the do function to be used with the http library.

//import the map function to be used with the http library


var ApplicationsaddComponent = /** @class */ (function () {
    function ApplicationsaddComponent(apiService, route, router) {
        this.apiService = apiService;
        this.route = route;
        this.router = router;
        this.bu = [
            { "id": 1, "name": "UKL" },
            { "id": 2, "name": "UKGI" },
            { "id": 3, "name": "GCUK" },
            { "id": 4, "name": "Global Life" }
        ];
        this.platform = [
            { "id": 0, "name": "Mainframe" },
            { "id": 1, "name": "Java" },
            { "id": 2, "name": ".Net" },
            { "id": 3, "name": "3rd Party" },
            { "id": 4, "name": "Lotus Notes" },
            { "id": 5, "name": "Server" }
        ];
        this.classification = [
            { "id": 0, "name": "Confidential" },
            { "id": 1, "name": "Highly Confidential" },
            { "id": 2, "name": "Internal Use Only" }
        ];
        this.scope = [
            { "id": 0, "name": "Primary & Special" },
            { "id": 1, "name": "Primary Only" },
            { "id": 2, "name": "Special Only" },
            { "id": 3, "name": "Out of scope" }
        ];
        this.primaryapplications = new __WEBPACK_IMPORTED_MODULE_5__models_applications__["a" /* Applications */]();
    }
    ApplicationsaddComponent.prototype.ngOnInit = function () {
    };
    ApplicationsaddComponent.prototype.addApplication = function () {
        var _this = this;
        this.apiService.addApplication(this.primaryapplications)
            .subscribe(function () { return _this.goBack(); });
    };
    ApplicationsaddComponent.prototype.goBack = function () {
        this.router.navigate(['/applications']);
    };
    ApplicationsaddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-applicationsadd',
            template: __webpack_require__("../../../../../src/app/components/applications/applicationsadd/applicationsadd.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/applications/applicationsadd/applicationsadd.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], ApplicationsaddComponent);
    return ApplicationsaddComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/applicationsadd.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsedit/applicationsedit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsedit/applicationsedit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Application Edit Form : You can edit an Application detail's information into this SAM Apps.</div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\" (submit)=\"updateApplication()\">\n<div class=\"form-group\">\n  <label for=\"appname\" class=\"col-sm-2 control-label\">Application Name : </label>\n  <div class=\"col-sm-3\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationName\" name=\"applicationName\" >\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"appowner\" class=\"col-sm-2 control-label\">Application Owner : </label>\n  <div class=\"col-sm-3\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationOwner\" name=\"applicationOwner\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"bu\" class=\"col-sm-2 control-label\">Business Unit : </label>\n  <div class=\"col-sm-2\">\n    <select [(ngModel)]=\"primaryapplications.bu\" name =\"primaryapplications.bu\" class=\"form-control\" style=\"height: 30px\">\n        <option  *ngFor=\"let product of bu\"\n          [value]=\"product.name\"\n         [attr.selected]=\"primaryapplications.bu==product.name ? true : null\">\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"platform\" class=\"col-sm-2 control-label\">Platform : </label>\n  <div class=\"col-sm-2\">\n    <select [(ngModel)]=\"primaryapplications.platform\" name =\"primaryapplications.platform\" class=\"form-control\" style=\"height: 30px\">\n        <option  *ngFor=\"let product of platform\"\n          [value]=\"product.name\"\n        [attr.selected]=\"primaryapplications.platform==product.name ? true : null\">\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"classification\" class=\"col-sm-2 control-label\">Classification : </label>\n  <div class=\"col-sm-3\">\n    <select [(ngModel)]=\"primaryapplications.classification\" name =\"primaryapplications.classification\" class=\"form-control\" style=\"height: 35px\">\n        <option  *ngFor=\"let product of classification\"\n          [value]=\"product.name\"\n        [attr.selected]=\"primaryapplications.classification==product.name ? true : null\">\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"scope\" class=\"col-sm-2 control-label\">Scope : </label>\n  <div class=\"col-sm-3\">\n    <select [(ngModel)]=\"primaryapplications.scope\" name =\"primaryapplications.scope\" class=\"form-control\" style=\"height: 35px\">\n        <option  *ngFor=\"let product of scope\"\n          [value]=\"product.name\"\n        >\n        {{product.name}}\n        </option>\n      </select>\n    </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"poc\" class=\"col-sm-2 control-label\">SAM Contact Person : </label>\n  <div class=\"col-sm-4\">\n    <input  type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.poc\" name=\"poc\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"comments\" class=\"col-sm-2 control-label\">Comments : </label>\n  <div class=\"col-sm-4\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.comments\" name=\"comments\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-2 col-sm-8\">\n    <button type=\"submit\" class=\"btn btn-success\">Update</button>\n     <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\n  </div>\n</div>\n</form>\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsedit/applicationsedit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationseditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_applications__ = __webpack_require__("../../../../../src/app/models/applications.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import component, ElementRef, input and the oninit method from angular core

//import the do function to be used with the http library.

//import the map function to be used with the http library


var ApplicationseditComponent = /** @class */ (function () {
    function ApplicationseditComponent(apiService, route, router) {
        this.apiService = apiService;
        this.route = route;
        this.router = router;
        this.bu = [
            { "id": 1, "name": "UKL" },
            { "id": 2, "name": "UKGI" },
            { "id": 3, "name": "GCUK" },
            { "id": 4, "name": "Global Life" }
        ];
        this.platform = [
            { "id": 0, "name": "Mainframe" },
            { "id": 1, "name": "Java" },
            { "id": 2, "name": ".Net" },
            { "id": 3, "name": "3rd Party" },
            { "id": 4, "name": "Lotus Notes" },
            { "id": 5, "name": "Server" }
        ];
        this.classification = [
            { "id": 0, "name": "Confidential" },
            { "id": 1, "name": "Highly Confidential" },
            { "id": 2, "name": "Internal Use Only" }
        ];
        this.scope = [
            { "id": 0, "name": "Primary & Special" },
            { "id": 1, "name": "Primary Only" },
            { "id": 2, "name": "Special Only" },
            { "id": 3, "name": "Out of scope" }
        ];
        this.title = 'Inscope Applications';
        this.primaryapplications = new __WEBPACK_IMPORTED_MODULE_5__models_applications__["a" /* Applications */];
        this.id = this.route.snapshot.params['id'];
    }
    ApplicationseditComponent.prototype.ngOnInit = function () {
        this.getApplication();
    };
    ApplicationseditComponent.prototype.getApplication = function () {
        var _this = this;
        this.apiService.getApplication(this.id)
            .subscribe(function (primaryapplications) {
            _this.primaryapplications = primaryapplications;
        });
    };
    ApplicationseditComponent.prototype.updateApplication = function () {
        var _this = this;
        this.apiService.updateApplication(this.id, this.primaryapplications)
            .subscribe(function () { return _this.goBack(); });
    };
    ApplicationseditComponent.prototype.goBack = function () {
        this.router.navigate(['/applications']);
    };
    ApplicationseditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-applicationsedit',
            template: __webpack_require__("../../../../../src/app/components/applications/applicationsedit/applicationsedit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/applications/applicationsedit/applicationsedit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], ApplicationseditComponent);
    return ApplicationseditComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/applicationsedit.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsshow/applicationsshow.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsshow/applicationsshow.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\" >\r\n<div class=\"panel-heading\">Application Details</div>\r\n<div class=\"panel-body\">\r\n  <form class=\"form-horizontal\" *ngIf=primaryapplications>\r\n<div class=\"form-group\">\r\n  <label for=\"applicationName\" class=\"col-sm-2 control-label\">Application Name : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\" >{{primaryapplications.applicationName}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"applicationOwner\" class=\"col-sm-2 control-label\">Application Owner : </label>\r\n  <div class=\"col-sm-9\">\r\n   <p class=\"form-control-static\">{{primaryapplications.applicationOwner}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"bu\" class=\"col-sm-2 control-label\">Business Unit : </label>\r\n  <div class=\"col-sm-9\">\r\n   <p class=\"form-control-static\">{{primaryapplications.bu}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"platform\" class=\"col-sm-2 control-label\">Platform : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\">{{primaryapplications.platform}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"platform\" class=\"col-sm-2 control-label\">Classification : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\">{{primaryapplications.classification}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"scope\" class=\"col-sm-2 control-label\">Scope : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\">{{primaryapplications.scope}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"poc\" class=\"col-sm-2 control-label\">SAM Contact Person : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\">{{primaryapplications.poc}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"platform\" class=\"col-sm-2 control-label\">Comments : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\">{{primaryapplications.comments}}</p>\r\n  </div>\r\n</div>\r\n<!-- <div class=\"form-group\">\r\n  <label for=\"id\" class=\"col-sm-2 control-label\">Applications's ID : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control-static\">08df088d89dsfhjf87</p>\r\n  </div>\r\n</div> -->\r\n<div class=\"form-group\">\r\n  <div class=\"col-sm-offset-2 col-sm-8\">\r\n    <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\r\n  </div>\r\n\r\n</div>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/applications/applicationsshow/applicationsshow.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationsshowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import component, ElementRef, input and the oninit method from angular core

//import the do function to be used with the http library.

//import the map function to be used with the http library

var ApplicationsshowComponent = /** @class */ (function () {
    function ApplicationsshowComponent(apiService, route, router) {
        this.apiService = apiService;
        this.route = route;
        this.router = router;
    }
    ApplicationsshowComponent.prototype.ngOnInit = function () {
        this.getApplication();
    };
    ApplicationsshowComponent.prototype.getApplication = function () {
        var _this = this;
        var id = this.route.snapshot.params['id'];
        this.apiService.getApplication(id)
            .subscribe(function (primaryapplications) {
            _this.primaryapplications = primaryapplications;
        });
    };
    ApplicationsshowComponent.prototype.goBack = function () {
        this.router.navigate(['applications']);
    };
    ApplicationsshowComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-applicationsshow',
            template: __webpack_require__("../../../../../src/app/components/applications/applicationsshow/applicationsshow.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/applications/applicationsshow/applicationsshow.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__services_api_service__["a" /* ApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], ApplicationsshowComponent);
    return ApplicationsshowComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/applicationsshow.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/changemanager/changemanager.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/changemanager/changemanager.component.html":
/***/ (function(module, exports) {

module.exports = "<p>Total Managers: {{ managers ? managers.length: '0' }}</p>\n<table class=\"table table-bordered\">\n\t\t  <thead>\n\t\t\t<tr>\n\t\t\t  <td>Manager Email</td>\n\t\t\t   <td width=\"275\" align=\"center\">Action</td>\n\t\t\t</tr>\n\t\t  </thead>\n\t\t  <tbody>\n\t\t\t <tr  *ngFor=\"let managerlist of managers\">\n\t\t\t\t<td>{{managerlist}}</td>\n\t\t\t\t<td width=\"275\">\n\t\t\t\t\t<a class=\"btn btn-success\"  routerLink=\"/editchangemanager/{{managerlist.managerlist}}\">Edit</a>\n\t\t\t\t</td>\n\t\t\t </tr>\n\t\t  </tbody>\n\t\t</table>\n"

/***/ }),

/***/ "../../../../../src/app/components/changemanager/changemanager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangemanagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_changemanager_service__ = __webpack_require__("../../../../../src/app/services/changemanager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChangemanagerComponent = /** @class */ (function () {
    function ChangemanagerComponent(changemanagerService, route, router) {
        this.changemanagerService = changemanagerService;
        this.route = route;
        this.router = router;
    }
    ChangemanagerComponent.prototype.ngOnInit = function () {
        this.getManagers();
    };
    ChangemanagerComponent.prototype.getManagers = function () {
        var _this = this;
        this.changemanagerService.getManagers()
            .subscribe(function (managers) {
            _this.managers = managers;
        });
    };
    ChangemanagerComponent.prototype.goBack = function () {
        this.router.navigate(['/changemanager']);
    };
    ChangemanagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-changemanager',
            template: __webpack_require__("../../../../../src/app/components/changemanager/changemanager.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/changemanager/changemanager.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_changemanager_service__["a" /* ChangemanagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_changemanager_service__["a" /* ChangemanagerService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], ChangemanagerComponent);
    return ChangemanagerComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/changemanager.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/changemanager/changemanageredit/changemanageredit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/changemanager/changemanageredit/changemanageredit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Manager Edit Form : You can edit an Manager's detail information into this SAM Apps.</div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\" (submit)=\"updateManager()\">\n<div class=\"form-group\">\n  <label for=\"mgr_name\" class=\"col-sm-2 control-label\">Manager Name : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" style='background-color:green;display:block;width:auto' class=\"form-control\" [(ngModel)]=\"managers\" name=\"managerName\" >\n  </div>\n</div>\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-2 col-sm-8\">\n    <button type=\"submit\" class=\"btn btn-success\">Update</button>\n     <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\n  </div>\n\n</div>\n</form>\n\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/changemanager/changemanageredit/changemanageredit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangemanagereditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChangemanagereditComponent = /** @class */ (function () {
    function ChangemanagereditComponent() {
    }
    ChangemanagereditComponent.prototype.ngOnInit = function () {
    };
    ChangemanagereditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-changemanageredit',
            template: __webpack_require__("../../../../../src/app/components/changemanager/changemanageredit/changemanageredit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/changemanager/changemanageredit/changemanageredit.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ChangemanagereditComponent);
    return ChangemanagereditComponent;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/changemanageredit.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Primary Dashboard</h2>\n<!-- <p>Dashboard under construction .... coming soon</p> -->\n<button (click)=\"exportToExcel()\" class=\"btn btn-primary\">Export to excel</button>\n<p>Total Applications: {{ apps ? apps.length-1: '0' }}</p>\n\n<table class=\"table table-bordered table-striped\" id=\"report\">\n     <thead>\n     <tr>\n       <!-- <th>ID</th> -->\n       <th align=\"center\">Application</th>\n       <th align=\"center\">Total Users</th>\n       <th align=\"center\">Pending</th>\n       <th align=\"center\">Recertified</th>\n       <th align=\"center\">Expired</th>\n       <th align=\"center\">Removals</th>\n       <th align=\"center\">Leavers</th>\n      <th align=\"center\">No Change</th>\n      <th align=\"center\">Moved to Special Cycle</th>\n     </tr>\n     </thead>\n     <tbody>\n       <tr  *ngFor=\"let primaryapplication of apps\">\n\t\t\t\t<td align=\"center\"><b>{{primaryapplication.application}}</b></td>\n        <td align=\"center\" ><p style=\"color:Blue\"><b>{{primaryapplication.total}}</b></p></td>\n        <td align=\"center\" ><p style=\"color:Red\"><b>{{primaryapplication.pending}}</b></p></td>\n        <td align=\"center\" ><p style=\"color:Brown\"><b>{{primaryapplication.recertified}}</b></p></td>\n        <td align=\"center\" ><p style=\"color:Green\">{{primaryapplication.expired}}</p></td>\n        <td align=\"center\" ><p style=\"color:Green\">{{primaryapplication.removals}}</p></td>\n        <td align=\"center\" ><p style=\"color:Green\">{{primaryapplication.leavers}}</p></td>\n        <td align=\"center\" ><p style=\"color:Green\">{{primaryapplication.noChange}}</p></td>\n        <td align=\"center\" ><p style=\"color:Green\">{{primaryapplication.movedToSpecial}}</p></td>\n     </tbody>\n   </table>\n"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_dashboard_service__ = __webpack_require__("../../../../../src/app/services/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(http, el, flashMessage, dashboardService, excelService, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.dashboardService = dashboardService;
        this.excelService = excelService;
        this.router = router;
        this.getUsersDashboard();
        this.excelService = excelService;
    }
    //Dashboard;//any[];
    //apps2 = new Dashboard();
    //apps2:Object;
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.getUsersDashboard = function () {
        var _this = this;
        this.dashboardService.getUsersDashboard()
            .subscribe(function (usersDashboard) {
            var len = usersDashboard.length;
            console.log("Implement filtering functionality here" + usersDashboard.length);
            var result = [];
            var item = [];
            var result = [];
            var application = [];
            var total = new Array(len).fill(0); //[] = 0;// [0];
            var recertified = new Array(len).fill(0);
            var expired = new Array(len).fill(0);
            var pending = new Array(len).fill(0);
            var removals = new Array(len).fill(0);
            var leavers = new Array(len).fill(0);
            var noChange = new Array(len).fill(0);
            var movedToSpecial = new Array(len).fill(0);
            var ttotal = 0;
            var trecertified = 0;
            var texpired = 0;
            var tpending = 0;
            var tremovals = 0;
            var tleavers = 0;
            var tnoChange = 0;
            var tmovedToSpecial = 0;
            //var apps2 = new Dashboard();
            var p = 0;
            for (var k = 0; k < len; k++) {
                item[k] = usersDashboard[k].application;
                //console.log(usersDashboard[k].application);
            }
            // console.log(" item length"+len);
            for (var i = 0; i < len; i++) {
                var isDistinct = false;
                for (var j = 0; j < i; j++) {
                    if (item[i] == item[j]) {
                        isDistinct = true;
                        break;
                    }
                }
                if (!isDistinct) {
                    result[p++] = item[i];
                    //  console.log(item[i]+" ");
                }
            }
            for (var i = 0; i < result.length; i++) {
                var appl;
                var sta;
                for (var k = 0; k < len; k++) {
                    appl = usersDashboard[k].application;
                    sta = usersDashboard[k].status;
                    if (result[i] == appl) {
                        total[i]++;
                        ttotal++;
                        if (sta == 'Expired') {
                            expired[i]++;
                            texpired++;
                            recertified[i]++;
                            trecertified++;
                        }
                        else if (sta == 'Not Recertified') {
                            pending[i]++;
                            tpending++;
                        }
                        else if (sta == 'Leavers') {
                            leavers[i]++;
                            tleavers++;
                            recertified[i]++;
                            trecertified++;
                        }
                        else if (sta == 'Removals') {
                            removals[i]++;
                            tremovals++;
                            recertified[i]++;
                            trecertified++;
                        }
                        else if (sta == 'No Change') {
                            noChange[i]++;
                            tnoChange++;
                            recertified[i]++;
                            trecertified++;
                        }
                        else if (sta == 'Moved to Special Cycle') {
                            movedToSpecial[i]++;
                            tmovedToSpecial++;
                            recertified[i]++;
                            trecertified++;
                        }
                        //else  if(sta != 'Not Recertified') {
                        //recertified[i]++;trecertified++;}
                    }
                }
            }
            delete usersDashboard.manager;
            usersDashboard["manager"] = null;
            delete usersDashboard["manager"];
            //console.log(usersDashboard.manager);
            _this.apps = usersDashboard;
            console.log("  this.apps :: keys" + _this.apps.keys());
            for (var i_1 in _this.apps) {
                _this.apps[i_1]._id = undefined;
                _this.apps[i_1].manager = undefined;
                _this.apps[i_1].logonid = undefined;
                _this.apps[i_1].status = undefined;
                _this.apps[i_1].surname = undefined;
                _this.apps[i_1].initials = undefined;
                _this.apps[i_1].userlocation = undefined;
                _this.apps[i_1].comments = undefined;
                _this.apps[i_1].application = undefined;
            }
            for (var key in _this.apps) {
                //console.log( "this.apps: keys"+key, this.apps[key] );
                delete _this.apps[key]._id;
                delete _this.apps[key].manager;
                delete _this.apps[key].logonid;
                delete _this.apps[key].status;
                delete _this.apps[key].surname;
                delete _this.apps[key].initials;
                delete _this.apps[key].userlocation;
                delete _this.apps[key].comments;
                delete _this.apps[key].application;
                //console.log( "this.apps: keys"+key, this.apps[key] );
            }
            //console.log(delete this.apps["manager"]);
            /*delete this.apps["manager"];
            this.apps["manager"]=null;
            this.apps["_id"] = null;
            this.apps["logonid"] = null;
            this.apps["status"] = null;
            this.apps["surname"] = null;
            this.apps["initials"] = null;
            this.apps["userlocation"] = null;
            this.apps["comments"] = null;*/
            //this.apps = [];
            _this.apps.length = (result.length + 1);
            var t = 0;
            for (var i_2 in _this.apps) {
                t++;
                if (t <= result.length) {
                    _this.apps[i_2].application = result[i_2];
                    _this.apps[i_2].total = total[i_2];
                    _this.apps[i_2].pending = pending[i_2];
                    _this.apps[i_2].recertified = recertified[i_2];
                    _this.apps[i_2].expired = expired[i_2];
                    _this.apps[i_2].removals = removals[i_2];
                    _this.apps[i_2].leavers = leavers[i_2];
                    _this.apps[i_2].noChange = noChange[i_2];
                    _this.apps[i_2].movedToSpecial = movedToSpecial[i_2];
                    /*  this.apps[i]._id = undefined;
                      this.apps[i].manager = undefined;
                      this.apps[i].logonid = undefined;
                      this.apps[i].status = undefined;
                      this.apps[i].surname = undefined;
                      this.apps[i].initials = undefined;
                      this.apps[i].userlocation = undefined;
                      this.apps[i].comments = undefined; */
                }
                else {
                    //this.apps[i] = 0;
                    _this.apps[i_2].application = "Total Counts";
                    _this.apps[i_2].total = ttotal;
                    _this.apps[i_2].expired = texpired;
                    _this.apps[i_2].recertified = trecertified;
                    _this.apps[i_2].pending = tpending;
                    _this.apps[i_2].leavers = tleavers;
                    _this.apps[i_2].removals = tremovals;
                    _this.apps[i_2].noChange = tnoChange;
                    _this.apps[i_2].movedToSpecial = tmovedToSpecial;
                    /*    this.apps[i]._id = undefined;
                        this.apps[i].manager = undefined;
                        this.apps[i].logonid = undefined;
                        this.apps[i].status = undefined;
                        this.apps[i].surname = undefined;
                        this.apps[i].initials = undefined;
                        this.apps[i].userlocation = undefined;
                        this.apps[i].comments = undefined; */
                }
            }
        });
    };
    DashboardComponent.prototype.exportToExcel = function (event) {
        /*this.apps2=this.apps;
      for (let i in this.apps) {
        this.apps2[i].application = this.apps[i].application;
        this.apps2[i].total = this.apps[i].total;
        this.apps2[i].expired = this.apps[i].expired;
        this.apps2[i].recertified = this.apps[i].recertified;
        this.apps2[i].pending = this.apps[i].pending;
        this.apps2[i].leavers = this.apps[i].leavers;
        this.apps2[i].removals = this.apps[i].removals;
        this.apps2[i].noChange = this.apps[i].noChange;
        this.apps2[i].movedToSpecial = this.apps[i].movedToSpecial;
        //this.apps[i]._id = undefined;
        this.apps[i].manager = undefined;
        this.apps[i].logonid = undefined;
        this.apps[i].status = undefined;
        this.apps[i].surname = undefined;
        this.apps[i].initials = undefined;
        this.apps[i].userlocation = undefined;
        this.apps[i].commments = undefined;
      } */
        this.excelService.exportAsExcelFile(this.apps, 'PrimaryDashboard');
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__services_dashboard_service__["a" /* DashboardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_dashboard_service__["a" /* DashboardService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _f || Object])
    ], DashboardComponent);
    return DashboardComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/delegatemanager/delegatemanager.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/delegatemanager/delegatemanager.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!-- <p>Total Managers: {{ listmanagers ? listmanagers.length: '0' }}</p>\n<select class=\"selectpicker\" data-live-search=\"true\" >\n  <option *ngFor=\"let manager of listmanagers\" value= {{manager._id}}>\n    {{manager.manager}}\n  </option>\n</select> -->\n<input type=\"hidden\" name=\"loggedmanager\" id=\"loggedmanager\"  value={{user}}/>\n<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Delegate Manager: <b>{{user}}</b> can delegate another manager to recertify users assigned to you.</div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\">\n<div class=\"form-group\">\n  <label for=\"mgr_name\" class=\"col-sm-2 control-label\">Manager Email ID:</label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"col-sm-6 form-control\" value={{dmanager}} name=\"delegatemanagerName\" id=\"delegatemanagerName\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-2 col-sm-8\">\n    <button class=\"btn btn-info\" id=\"save\" (click)=\"updateManager()\">Save</button>\n    <button class=\"btn btn-danger\"  id=\"remove\" (click)=\"deleteDelegation()\">Remove</button>\n    <a class=\"btn btn-success\" (click)=\"goBackScreen()\">Back</a>\n  </div>\n</div>\n</form>\n\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/delegatemanager/delegatemanager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DelegatemanagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__ = __webpack_require__("../../../../../src/app/services/managerextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DelegatemanagerComponent = /** @class */ (function () {
    function DelegatemanagerComponent(http, el, flashMessage, managerextractService, excelService, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.managerextractService = managerextractService;
        this.excelService = excelService;
        this.router = router;
        this.getManagers();
        this.excelService = excelService;
        var url = document.location.href;
        var temp = url.split('?');
        for (var i = 0; i < temp.length; i++) {
            var qval2 = temp[i].split("=");
            var qval = qval2[i];
            qval = qval.replace("%40", "@");
            // console.log(qval);
            this.user = qval;
            //document.getElementById(qval[0]).value = qval[1];
        }
        this.getDelegateManager();
    }
    DelegatemanagerComponent.prototype.ngOnInit = function () {
    };
    DelegatemanagerComponent.prototype.getManagers = function () {
        var _this = this;
        this.managerextractService.getManagers()
            .subscribe(function (listmanagers) {
            _this.listmanagers = listmanagers;
            /*  this.list = listmanagers;
              for (var key in this.list){
                //console.log( "this.apps: keys"+key, this.apps[key] );
                delete this.list[key]._id;
                //console.log( "this.apps: keys"+key, this.apps[key] );
              }*/
            //console.log(listmanagers);
        });
    };
    DelegatemanagerComponent.prototype.updateManager = function () {
        var _this = this;
        var oldmanager = document.getElementById('loggedmanager').value;
        console.log("delegate Manager form submit::" + oldmanager);
        var newmanager = document.getElementById('delegatemanagerName').value;
        console.log("delegate Manager form submit::" + newmanager);
        this.managerextractService.delegateManagers(oldmanager, newmanager)
            .subscribe(function () { return _this.goBack(); });
        this.flashMessage.show('Delegation updated successfully', { cssClass: 'alert-success', timeout: 3000 });
    };
    DelegatemanagerComponent.prototype.exportToExcel = function (event) {
        this.excelService.exportAsExcelFile(this.listmanagers, 'listmanagers');
    };
    DelegatemanagerComponent.prototype.goBack = function () {
        //window.location.reload();
        //this.router.navigate(['/delegatemanager'])
    };
    DelegatemanagerComponent.prototype.goBackScreen = function () {
        window.history.back();
    };
    DelegatemanagerComponent.prototype.deleteDelegation = function () {
        var _this = this;
        var oldmanager = document.getElementById('loggedmanager').value;
        console.log("delegate Manager form submit::" + oldmanager);
        var newmanager = " ";
        console.log("delegate Manager form submit::" + newmanager);
        this.managerextractService.deleteDelegateManagers(oldmanager)
            .subscribe(function () { return _this.goBack(); });
        this.flashMessage.show('Delegation deleted successfully', { cssClass: 'alert-success', timeout: 3000 });
        document.getElementById('delegatemanagerName').value = "";
    };
    DelegatemanagerComponent.prototype.getDelegateManager = function () {
        var _this = this;
        console.log("this.user::" + this.user);
        this.managerextractService.getDelegateManager(this.user)
            .subscribe(function (listmanagers) {
            _this.listmanagers = listmanagers;
            console.log("this.listmanagers::" + _this.listmanagers);
            //if(this.listmanagers["_id"]!=null)
            if (_this.listmanagers) {
                for (var key in _this.listmanagers) {
                    console.log("this.listmanagers: keys" + key, _this.listmanagers[key]);
                    if (_this.listmanagers[key].length < 1)
                        _this.listmanagers[key] = "";
                }
                _this.dmanager = _this.listmanagers["dmanager"];
                console.log("damanger:" + _this.dmanager);
            }
            else {
                //var msg =
                //document.getElementById('delegatemanagerName').color.disabled=true;
                document.getElementById('delegatemanagerName').value = "This option is available for Managers only";
                document.getElementById('delegatemanagerName').disabled = true;
                document.getElementById('save').disabled = true;
                document.getElementById('remove').disabled = true;
            }
        });
    };
    DelegatemanagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-delegatemanager',
            template: __webpack_require__("../../../../../src/app/components/delegatemanager/delegatemanager.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/delegatemanager/delegatemanager.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__["a" /* ManagerextractService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__["a" /* ManagerextractService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_excel_service__["a" /* ExcelService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _f || Object])
    ], DelegatemanagerComponent);
    return DelegatemanagerComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/delegatemanager.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-center\">\n  <h1>SAM Recertification</h1>\n  <p>Welcome to Security Access Management (SAM) Recertification Annual Exercise</p>\n  </div>\n<div class=\"row\">\n  <div class=\"col-md-4\">\n    <h3>Objective</h3>\n    <p style=\"font-size:14px\">The main objective of SAM Recertification Process is to validate users who have primary access and special access to the in scope applications, with the exception of the LDC Web Applications</p>\n    <h3>Primary access :</h3>\n    <p style=\"font-size:14px\">Means validating the user who has access to which applications and removing any unnecessary users. This includes mainly business users to front end systems (and needs to cover everyone who has access to the application, be it update, read-only etc).</p>\n  </div>\n  <div class=\"col-md-4\">\n    <h3>Special access :</h3>\n    <p style=\"font-size:14px\">Means validate users who have been deemed as having Special Access under the following definitions agreed with ICF in 2007. Below are the definitions (1-4):-</p>\n      <p>1.\tUpdate code in Production</p>\n      <p>2.\tUpdate Production database or data directly</p>\n      <p>3.\tRights to move code to Production</p>\n      <p>4.\tAccess  Role Profile creation / amendment / deletion and System Administrators who have access to Configuration Data.</p>\n  </div>\n  <div class=\"col-md-4\">\n    <h3>Recertification Categories:</h3>\n    <b>No change:</b>- Use this category if the access is still required.<br/><br/>\n    <b>Leaver:</b>- Use this category if the employee has left Zurich, or left Zurich's account. <br/>\n    Please note: This will trigger removal of all accesses including their GAD or EZCORP account <br/>\n    Please do not use this category for people who have moved internally within the Zurich Business.\n    <br/><br/>\n    <b>Removal:</b>- Use this category if access is no longer required to the application for the specified login ID. <br/>\n    This category can also be used to remove access to applications where the employee has moved teams and no longer requires access to application.<br/>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        var url = document.location.href;
        var temp = url.split('?');
        //temp = temp.split('='); // Now we have all the values in an array, with each element looking like "var=val";
        for (var i = 0; i < temp.length; i++) {
            var qval2 = temp[i].split("=");
            var qval = qval2[i];
            qval = qval.replace("%40", "@");
            // console.log(qval);
            this.user = qval;
            //document.getElementById(qval[0]).value = qval[1];
        }
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/home.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "form {\r\n    width: 300px;\r\n    margin: 0 auto;\r\n}\r\n#username {\r\n  text-transform: uppercase;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n\n\n<form (submit)=\"onLoginSubmit()\">\n  <h2 class=\"page-header\">Manager Login</h2>\n    <label>Email Id:</label>\n    <input type=\"text\" id=\"username\"  [(ngModel)]=\"username\" name=\"username\" ng-change=\"username=username.toUpperCase();\">\n    <input type=\"submit\" class=\"btn btn-primary\" value=\"Login\">\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        this.authService.authenticateUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user, user.username);
                console.log("length::" + user.username);
                //console.log("username::"+data.user.username);
                //console.log("manager::"+data.user.manager);
                _this.flashMessage.show('You are now logged in', {
                    cssClass: 'alert-success',
                    timeout: 5000
                });
                //this.router.navigate(['output']);
                _this.router.navigate(['/output'], { queryParams: { user: user.username } });
                //this.router.navigate(['/userextracts'], { queryParams: { user: user.username} });
            }
            else {
                _this.flashMessage.show(data.msg, {
                    cssClass: 'alert-danger',
                    timeout: 5000
                });
                _this.router.navigate(['login']);
            }
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/login.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/managerchange/managerchange.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/managerchange/managerchange.component.html":
/***/ (function(module, exports) {

module.exports = "<h4>\n<!-- here we echo the title from the component -->\n  <b>List of Managers</b> <button (click)=\"exportToExcel()\" class=\"btn btn-primary\">Export to excel</button>\n</h4>\n\n<p>Total Managers: {{ listmanagers ? listmanagers.length: '0' }}</p>\n<table class=\"table table-bordered\">\n\t\t  <thead>\n\t\t\t<tr>\n\t\t\t  <th width=\"40%\">Manager Email - ID</th>\n\t\t\t  <th width=\"10%\" align=\"center\">Action</th>\n\t\t\t\t\t</tr>\n\t\t  </thead>\n\t\t  <tbody>\n\t\t\t\t<tr  *ngFor=\"let manager of listmanagers | paginate: {itemsPerPage: 10, currentPage:page, id: '1'}; let i = index\">\n\t\t\t\t<td width=\"40%\">{{manager.manager}}</td>\n\t\t\t\t<td width=\"10%\">\n\t\t\t\t\t<a class=\"btn btn-success btn-sm\"  routerLink=\"/managerchangeedit/{{manager._id}}\">Edit</a>\n\t\t\t\t</td>\n\t\t\t\t </tr>\n\t\t  </tbody>\n\t\t</table>\n\t\t<div class=\"centre\">\n\t<pagination-controls class=\"pagination-lg\" (pageChange)=\"page = $event\" id=\"1\"\n\t\t\t\t\t\t\tmaxSize=\"10\"\n\t\t\t\t\t\t\tdirectionLinks=\"true\"\n\t\t\t\t\t\t\tautoHide=\"true\">\n\t\t\t\t</pagination-controls>`\n\t</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/managerchange/managerchange.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerchangeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__ = __webpack_require__("../../../../../src/app/services/managerextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ManagerchangeComponent = /** @class */ (function () {
    function ManagerchangeComponent(http, el, flashMessage, managerextractService, excelService, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.managerextractService = managerextractService;
        this.excelService = excelService;
        this.router = router;
        this.getManagers();
        this.excelService = excelService;
    }
    ManagerchangeComponent.prototype.ngOnInit = function () {
    };
    ManagerchangeComponent.prototype.getManagers = function () {
        var _this = this;
        this.managerextractService.getManagers()
            .subscribe(function (listmanagers) {
            _this.listmanagers = listmanagers;
            /*  this.list = listmanagers;
              for (var key in this.list){
                //console.log( "this.apps: keys"+key, this.apps[key] );
                delete this.list[key]._id;
                //console.log( "this.apps: keys"+key, this.apps[key] );
              }*/
            //console.log(listmanagers);
        });
    };
    ManagerchangeComponent.prototype.exportToExcel = function (event) {
        this.excelService.exportAsExcelFile(this.listmanagers, 'listmanagers');
    };
    ManagerchangeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-managerchange',
            template: __webpack_require__("../../../../../src/app/components/managerchange/managerchange.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/managerchange/managerchange.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__["a" /* ManagerextractService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__["a" /* ManagerextractService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_excel_service__["a" /* ExcelService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _f || Object])
    ], ManagerchangeComponent);
    return ManagerchangeComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerchange.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/managerchange/managerchangeedit/managerchangeedit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/managerchange/managerchangeedit/managerchangeedit.component.html":
/***/ (function(module, exports) {

module.exports = "<h4>\n<!-- here we echo the title from the component -->\n  <b>{{title}}</b>\n</h4>\n\n<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Manager Edit Form : You can edit an Manager's detail information into this SAM Apps.</div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\" (submit)=\"updateManager()\">\n<div class=\"form-group\">\n  <label for=\"mgr_name\" class=\"col-sm-2 control-label\">Manager Email - ID : </label>\n  <div class=\"col-sm-4\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"manager.manager\" name=\"manager\" >\n  </div>\n</div>\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-1 col-sm-2\">\n    <button type=\"submit\" class=\"btn btn-success\">Update</button>\n     <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\n  </div>\n\n</div>\n</form>\n\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/managerchange/managerchangeedit/managerchangeedit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerchangeeditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__ = __webpack_require__("../../../../../src/app/services/managerextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_managerchange__ = __webpack_require__("../../../../../src/app/models/managerchange.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ManagerchangeeditComponent = /** @class */ (function () {
    function ManagerchangeeditComponent(http, el, route, flashMessage, managerextractService, router) {
        this.http = http;
        this.el = el;
        this.route = route;
        this.flashMessage = flashMessage;
        this.managerextractService = managerextractService;
        this.router = router;
        this.id = this.route.snapshot.params['id'];
        this.title = 'Update Manager Email ID';
        this.manager = new __WEBPACK_IMPORTED_MODULE_6__models_managerchange__["a" /* ManagerChange */];
        this.getManager();
    }
    ManagerchangeeditComponent.prototype.ngOnInit = function () {
        this.getManager();
    };
    ManagerchangeeditComponent.prototype.getManager = function () {
        var _this = this;
        this.managerextractService.getManager(this.id)
            .subscribe(function (manager) {
            _this.manager = manager;
        });
    };
    ManagerchangeeditComponent.prototype.updateManager = function () {
        var _this = this;
        this.managerextractService.updateManager(this.id, this.manager)
            .subscribe(function () { return _this.goBack(); });
        this.flashMessage.show('Manager updated successfully', { cssClass: 'alert-success', timeout: 3000 });
    };
    ManagerchangeeditComponent.prototype.goBack = function () {
        this.router.navigate(['/managerchange']);
    };
    ManagerchangeeditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-managerchangeedit',
            template: __webpack_require__("../../../../../src/app/components/managerchange/managerchangeedit/managerchangeedit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/managerchange/managerchangeedit/managerchangeedit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__["a" /* ManagerextractService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_managerextract_service__["a" /* ManagerextractService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _f || Object])
    ], ManagerchangeeditComponent);
    return ManagerchangeeditComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerchangeedit.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/managerdashboard/managerdashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/managerdashboard/managerdashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<p>Hi {{user}} </p>\r\n\r\n<p>Total Number of users : {{ data ? data.length: '0' }}</p>\r\n<form (submit)=\"onSubmit()\">\r\n        <table class=\"table table-bordered\">\r\n             <thead>\r\n             <tr>\r\n               <td>Manager</td>\r\n               <td>Surname</td>\r\n               <td>Initials</td>\r\n               <td>LogonID</td>\r\n               <td>User Location/Region</td>\r\n               <td>Application</td>\r\n               <td>status</td>\r\n             </tr>\r\n           </thead>\r\n             <tbody>\r\n              <tr  *ngFor=\"let manager of primarymanagers\">\r\n               <td>{{manager.manager}}</td>\r\n               <td>{{manager.surname}}</td>\r\n               <td>{{manager.initials}}</td>\r\n               <td>{{manager.logonid}}</td>\r\n               <td>{{manager.userlocation}}</td>\r\n               <td>{{manager.application}}</td>\r\n               <td><select name=\"singleSelect\" ng-model=\"data.singleSelect\">\r\n     <option value=\"option-1\">Not Recertified</option>\r\n     <option value=\"option-2\">No Change</option>\r\n     <option value=\"option-2\">Leavers</option>\r\n     <option value=\"option-2\">Removals</option>\r\n   </select><br></td>\r\n              </tr>\r\n             </tbody>\r\n           </table>\r\n<input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">\r\n<a (click)=\"onLogoutClick()\" href=\"#\">Logout</a>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/managerdashboard/managerdashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerdashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_uploadmanager_service__ = __webpack_require__("../../../../../src/app/services/uploadmanager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ManagerdashboardComponent = /** @class */ (function () {
    function ManagerdashboardComponent(uploadmanagerService, route, flashMessage, router) {
        this.uploadmanagerService = uploadmanagerService;
        this.route = route;
        this.flashMessage = flashMessage;
        this.router = router;
        //this.route.params.subscribe( params => console.log(params) );
    }
    ManagerdashboardComponent.prototype.ngOnInit = function () {
        //this.getUploadmanagers();
        //console.log(user);
        var url = document.location.href;
        var temp = url.split('?');
        //temp = temp.split('='); // Now we have all the values in an array, with each element looking like "var=val";
        for (var i = 0; i < temp.length; i++) {
            var qval2 = temp[i].split("=");
            var qval = qval2[i];
            qval = qval.replace("%40", "@");
            console.log(qval);
            this.user = qval;
            //document.getElementById(qval[0]).value = qval[1];
        }
        console.log("this.user::" + this.user);
    };
    ManagerdashboardComponent.prototype.getUploadmanagers = function () {
        var _this = this;
        this.uploadmanagerService.getUploadmanagers()
            .subscribe(function (primarymanagers) {
            _this.primarymanagers = primarymanagers;
            console.log(_this.primarymanagers);
        });
    };
    ManagerdashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-managerdashboard',
            template: __webpack_require__("../../../../../src/app/components/managerdashboard/managerdashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/managerdashboard/managerdashboard.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_uploadmanager_service__["a" /* UploadmanagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_uploadmanager_service__["a" /* UploadmanagerService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _d || Object])
    ], ManagerdashboardComponent);
    return ManagerdashboardComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerdashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/managerexxtract/managerexxtract.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/managerexxtract/managerexxtract.component.html":
/***/ (function(module, exports) {

module.exports = "<h4>\r\n<!-- here we echo the title from the component -->\r\n  <b>{{title}}</b>\r\n</h4>\r\n\r\n<input type=\"file\" name=\"photo\" ng2FileSelect [uploader]=\"uploader\" accept=\".xlsx, .xls\" multiple />\r\n   <button type=\"button\" class=\"btn btn-success btn-s\" [disabled]=\"!uploader.getNotUploadedItems().length\"\r\n                   (click)=\"uploader.uploadAll();\">\r\n           <span class=\"glyphicon glyphicon-upload\"></span> Upload Manager Extracts\r\n         </button>\r\n         <button (click)=\"exportToExcel()\" class=\"btn btn-primary\">Export to excel</button>\r\n         <br />\r\n <p>Total Number of users uploaded: {{ primarymanagers ? primarymanagers.length: '0' }}</p>\r\n\r\n         <table class=\"table table-bordered table-striped\">\r\n         \t\t  <thead>\r\n         \t\t\t<tr>\r\n         \t\t\t  <th>Manager</th>\r\n         \t\t\t  <th>Resource</th>\r\n         \t\t\t  <th>Initials</th>\r\n                <th>LogonId</th>\r\n                <th>User Location/Region</th>\r\n                <th>Application</th>\r\n         \t\t\t</tr>\r\n         \t\t  </thead>\r\n         \t\t  <tbody>\r\n         \t\t\t <tr  *ngFor=\"let manager of primarymanagers | paginate: {itemsPerPage: 10, currentPage:page, id: '1'}; let i = index\">\r\n         \t\t\t\t<td>{{manager.manager}}</td>\r\n         \t\t\t\t<td>{{manager.resource}}</td>\r\n         \t\t\t\t<td>{{manager.initials}}</td>\r\n                <td>{{manager.logonid}}</td>\r\n                <td>{{manager.userlocation}}</td>\r\n                <td>{{manager.application}}</td>\r\n         \t\t\t </tr>\r\n               </tbody>\r\n               <tr *ngIf=\"! primarymanagers\">\r\n                <td colspan=\"6\" style=\"text-align: center;\">No Records exist</td>\r\n                </tr>\r\n         \t\t</table>\r\n\r\n            <div class=\"centre\">\r\n    <pagination-controls class=\"pagination-lg\" (pageChange)=\"page = $event\" id=\"1\"\r\n                      maxSize=\"10\"\r\n                      directionLinks=\"true\"\r\n                      autoHide=\"true\">\r\n                </pagination-controls>`\r\n   </div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/managerexxtract/managerexxtract.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerexxtractComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_managerextract_service__ = __webpack_require__("../../../../../src/app/services/managerextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var URL = 'http://localhost:8080/api/managerextract';
var ManagerexxtractComponent = /** @class */ (function () {
    function ManagerexxtractComponent(http, el, flashMessage, managerextractService, excelService, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.managerextractService = managerextractService;
        this.excelService = excelService;
        this.router = router;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: URL, itemAlias: 'photo' });
        this.title = 'Manager Extracts';
        // this.getManagerExtracts();
        this.excelService = excelService;
    }
    ManagerexxtractComponent.prototype.ngOnInit = function () {
        var _this = this;
        //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
        this.uploader.onAfterAddingFile = function (file) { file.withCredentials = false; };
        //overide the onCompleteItem property of the uploader so we are
        //able to deal with the server response.
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            console.log("ImageUpload:uploaded:", item, status, response);
            var responsePath = JSON.parse(response);
            console.log("ImageUpload--> completion:", response, responsePath); // the url will be in the response
            _this.flashMessage.show('Files uploaded successfully', { cssClass: 'alert-success', timeout: 3000 });
            //this.router.navigate(['/managerextracts']);
            //window.location.reload();
        };
        this.uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
            window.location.reload();
        };
    };
    //the function which handles the file upload without using a plugin.
    ManagerexxtractComponent.prototype.upload = function () {
        //locate the file element meant for the file upload.
        var inputEl = this.el.nativeElement.querySelector('#photo');
        //get the total amount of files attached to the file input.
        var fileCount = inputEl.files.length;
        //create a new fromdata instance
        var formData = new FormData();
        //check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) {
            //append the key name 'photo' with the first file in the element
            formData.append('photo', inputEl.files.item(0));
            //call the angular http method
            this.http
                .post(URL, formData).map(function (res) { return res.json(); }).subscribe(
            //map the success function and alert the response
            function (success) {
                alert(success._body);
                //this.clearSearch();
                //window.location.reload();
                //))
            }, function (error) { return alert(error); });
        }
        //window.location.reload();
    };
    ManagerexxtractComponent.prototype.clearSearch = function () {
        window.location.reload();
    };
    /*  getManagerExtracts(){
          this.managerextractService.getManagerExtracts()
              .subscribe(primarymanagers=>{
                this.primarymanagers = primarymanagers;
                this.primary = primarymanagers;

                for (var key in this.primary){
                  //console.log( "this.apps: keys"+key, this.apps[key] );
                  delete this.primary[key]._id;
                  //console.log( "this.apps: keys"+key, this.apps[key] );
                }
            });
        } */
    ManagerexxtractComponent.prototype.exportToExcel = function (event) {
        this.excelService.exportAsExcelFile(this.primary, 'primarymanagers');
    };
    ManagerexxtractComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-managerexxtract',
            template: __webpack_require__("../../../../../src/app/components/managerexxtract/managerexxtract.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/managerexxtract/managerexxtract.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__services_managerextract_service__["a" /* ManagerextractService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_managerextract_service__["a" /* ManagerextractService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _f || Object])
    ], ManagerexxtractComponent);
    return ManagerexxtractComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerexxtract.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/managerupload/managerupload.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/managerupload/managerupload.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"file\" class=\"form-control\" name=\"single\" ng2FileSelect [uploader]=\"uploader\"/>\r\n   <button type=\"button\" class=\"btn btn-success btn-s\"\r\n                 (click)=\"uploader.uploadAll();\">\r\n           <span class=\"glyphicon glyphicon-upload\"></span> Upload all\r\n         </button><br />\r\n <p>Total Number of users uploaded: {{ primarymanagers ? primarymanagers.length: '0' }}</p>\r\n\r\n         <table class=\"table table-bordered\">\r\n         \t\t  <thead>\r\n         \t\t\t<tr>\r\n         \t\t\t  <td>Manager</td>\r\n         \t\t\t  <td>Surname</td>\r\n         \t\t\t  <td>Initials</td>\r\n                <td>LogonID</td>\r\n                <td>User Location/Region</td>\r\n                <td>Application</td>\r\n         \t\t\t</tr>\r\n         \t\t  </thead>\r\n         \t\t  <tbody>\r\n         \t\t\t <tr  *ngFor=\"let manager of primarymanagers\">\r\n         \t\t\t\t<td>{{manager.manager}}</td>\r\n         \t\t\t\t<td>{{manager.surname}}</td>\r\n         \t\t\t\t<td>{{manager.initials}}</td>\r\n                <td>{{manager.logonid}}</td>\r\n                <td>{{manager.userlocation}}</td>\r\n                <td>{{manager.application}}</td>\r\n         \t\t\t </tr>\r\n         \t\t  </tbody>\r\n         \t\t</table>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/managerupload/managerupload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManageruploadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_uploadmanager_service__ = __webpack_require__("../../../../../src/app/services/uploadmanager.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ManageruploadComponent = /** @class */ (function () {
    function ManageruploadComponent(uploadmanagerService, route, router) {
        this.uploadmanagerService = uploadmanagerService;
        this.route = route;
        this.router = router;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: "http://localhost:8080/api/uploadmanager/" });
    }
    ManageruploadComponent.prototype.ngOnInit = function () {
        this.getUploadmanagers();
    };
    ManageruploadComponent.prototype.getUploadmanagers = function () {
        var _this = this;
        this.uploadmanagerService.getUploadmanagers()
            .subscribe(function (primarymanagers) {
            _this.primarymanagers = primarymanagers;
            console.log(_this.primarymanagers);
        });
    };
    ManageruploadComponent.prototype.goBack = function () {
        this.router.navigate(['/mhome']);
    };
    ManageruploadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-managerupload',
            template: __webpack_require__("../../../../../src/app/components/managerupload/managerupload.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/managerupload/managerupload.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_uploadmanager_service__["a" /* UploadmanagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_uploadmanager_service__["a" /* UploadmanagerService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], ManageruploadComponent);
    return ManageruploadComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerupload.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/mlogin/mlogin.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "form {\r\n    width: 300px;\r\n    margin: 0 auto;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/mlogin/mlogin.component.html":
/***/ (function(module, exports) {

module.exports = "\n<form (submit)=\"onLoginSubmit()\">\n  <h2 class=\"page-header\">Policy Search </h2>\n  <div class=\"form-group\">\n    <label>Provide Policy number</label>\n    <input style='display:block;width:auto' type=\"text\" class=\"form-control\" [(ngModel)]=\"email\" name=\"email\">\n  </div>\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"SearchDB\">\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/components/mlogin/mlogin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MloginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_mlogin_service__ = __webpack_require__("../../../../../src/app/services/mlogin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MloginComponent = /** @class */ (function () {
    function MloginComponent(mloginService, router, route, flashMessage) {
        this.mloginService = mloginService;
        this.router = router;
        this.route = route;
        this.flashMessage = flashMessage;
        this.email = this.route.snapshot.params['email'];
    }
    MloginComponent.prototype.ngOnInit = function () {
    };
    MloginComponent.prototype.onLoginSubmit = function () {
        //this.mloginService.authenticateManager(this.email).subscribe(data => {
        //  console.log('client',+this.email);
        //if(data.success){
        //this.mloginService.storeUserData(data.token, data.user);
        this.flashMessage.show('You are now logged in', {
            cssClass: 'alert-success',
            timeout: 5000
        });
        //  this.router.navigate(['dashboard2']);
        //} else {
        //  this.flashMessage.show(data.msg, {
        //    cssClass: 'alert-danger',
        //    timeout: 5000});
        this.router.navigate(['mdashboard']);
        //}
        //});
    };
    MloginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-mlogin',
            template: __webpack_require__("../../../../../src/app/components/mlogin/mlogin.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/mlogin/mlogin.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_mlogin_service__["a" /* MloginService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_mlogin_service__["a" /* MloginService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]) === "function" && _d || Object])
    ], MloginComponent);
    return MloginComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/mlogin.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<img src=\"/assets/dxc.png\" alt=\"Mountain View\" height=\"50\" width=\"20%\">\r\n<img src=\"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D\" width=\"10%\" height=\"0\" alt=\"\" />\r\n<img src=\"/assets/sam.png\" alt=\"dxc logo\" height=\"50\" width=\"25%\"/>\r\n<img src=\"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D\" width=\"10%\" height=\"0\" alt=\"\" />\r\n<img src=\"/assets/zurich.png\" alt=\"dxc logo\" align=\"right\" height=\"50\" width=\"20%\"/>\r\n<img src=\"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D\" width=\"5%\" height=\"0\" alt=\"\" />\r\n\r\n<nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">\r\n      <!-- <a class=\"navbar-brand\" href=\"#\">Titian POLO Reconciliation</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\r\n        <ul class=\"navbar-nav mr-auto\">\r\n          <li *ngIf=\"!authService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\">\r\n            <a class=\"nav-link\" (click)=\"onHome()\">Search</a>\r\n          </li>\r\n          <li  *ngIf=\"!authService.loggedIn()\" sclass=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\" >\r\n              <a class=\"nav-link\" (click)=\"onClient()\">Process</a>\r\n\r\n\t\t  </li> -->\r\n\t\t  <a class=\"navbar-brand\" href=\"#\">SAM Recertification</a>\r\n\t\t  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n\t\t\t<span class=\"navbar-toggler-icon\"></span>\r\n\t\t  </button>\r\n\t\r\n\t\t  <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\r\n\t\t\t<ul class=\"navbar-nav mr-auto\">\r\n\t\t\t  <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\">\r\n\t\t\t\t<a class=\"nav-link\" [routerLink]=\"['/']\">Home</a>\r\n\t\t\t  </li>\r\n\t\t\t  <li *ngIf=\"authService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\">\r\n\t\t\t\t<a class=\"nav-link\" [routerLink]=\"['/output']\"> Primary Cycle</a>\r\n\t\t\t  </li>\r\n\t\t\t  <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\"  [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\">\r\n\t\t\t\t  <a class=\"nav-link\" (click)=\"onDelegate()\" style=\"cursor: pointer;\">Delegate Manager</a>\r\n\t\t\t</li>\r\n\r\n\r\n          <!--[routerLink]=\"['/output']\"  [routerLink]=\"['/']\" -->\r\n\r\n         <!-- <div class=\"navbar-nav navbar-left dropdown\" dropdown>\r\n\t\t\t\t<li  *ngIf=\"authService.loggedIn()\" sclass=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\" ><a class=\"nav-link\" dropdown-open>Primary Cycle<b class=\"caret\"></b></a></li>\r\n\t\t\t\t<ul class=\"dropdown-menu\">\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/applications']\"> Applications In-Scope</a></li> \r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/userextracts']\"> User Extracts</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/managerextracts']\"> Manager Extracts</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/managerchange']\">4. Update Manager Email-ID</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/output']\"> Recertification</a></li>\r\n\t\t\t\t</ul>\r\n      </div> -->\r\n\r\n    <!--    <div class=\"navbar-nav navbar-left dropdown\" dropdown>\r\n\t\t\t\t<li *ngIf=\"authService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\" ><a class=\"nav-link\" dropdown-open>Special Cycle I<b class=\"caret\"></b></a></li>\r\n\t\t\t\t<ul class=\"dropdown-menu\">\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/applications']\">1. Applications In-Scope</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/userextracts']\">2. User Extracts</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/managerextracts']\">3. Manager Extracts</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/managerchange']\">4. Update Manager Email-ID</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/output']\">5. Recertification</a></li>\r\n\t\t\t\t</ul>\r\n\t\t    </div>\r\n\r\n        <div class=\"navbar-nav navbar-left dropdown\" dropdown>\r\n\t\t\t\t<li *ngIf=\"authService.loggedIn()\" class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\" ><a class=\"nav-link\" dropdown-open>Special Cycle II<b class=\"caret\"></b></a></li>\r\n\t\t\t\t<ul class=\"dropdown-menu\">\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/applications']\">1. Applications In-Scope</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/userextracts']\">2. User Extracts</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/managerextracts']\">3. Manager Extracts</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/managerchange']\">4. Update Manager Email-ID</a></li>\r\n\t\t\t\t\t\t<li><a [routerLink]=\"['/output']\">5. Recertification</a></li>\r\n\t\t\t\t</ul>\r\n      </div> -->\r\n        </ul>\r\n\r\n      <ul class=\"navbar-nav navbar-right\">\r\n\t\t\t<li class=\"nav-item\" *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\"><a class=\"nav-link\" [routerLink]=\"['/login']\" >Manager Login</a></li>\r\n\t\t\t<li class=\"nav-item\" *ngIf=\"authService.loggedIn()\"><a class=\"nav-link\" (click)=\"onLogoutClick()\" href=\"#\">Logout</a></li>      \t\t\r\n\t\t</ul>\r\n\r\n      </div>\r\n    </nav>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onHome = function () {
        //console.log("from navbar component");
        var temp = document.getElementById('loggedmanager').value;
        console.log("from navbar::home component::" + temp);
        this.router.navigate(['/'], { queryParams: { user: temp } });
    };
    NavbarComponent.prototype.onDelegate = function () {
        var temp = this.authService.getUser();
        console.log("from navbar::home component::" + temp);
        this.router.navigate(['/delegate'], { queryParams: { user: temp } });
    };
    NavbarComponent.prototype.onClient = function () {
        //console.log("from navbar component");
        var temp = document.getElementById('loggedmanager').value;
        console.log("from navbar:; primary component::" + temp);
        this.router.navigate(['/output']);
        // this.router.navigate(['/output'], { queryParams: { user: temp } });
        //<input type="text" name="loggedmanager" value={{user}}/>
        /*const user = {
          username: this.username,
          password: this.password
        }
      
        this.authService.authenticateUser(user).subscribe(data => {
          if(data.success){
            this.authService.storeUserData(data.token, data.user);
            console.log("length::"+user.username);
            //console.log("username::"+data.user.username);
            //console.log("manager::"+data.user.manager);
            this.flashMessage.show('You are now logged in', {
              cssClass: 'alert-success',
              timeout: 5000});
            //this.router.navigate(['output']);
            this.router.navigate(['/output'], { queryParams: { user: user.username } });
          }
        }); */
        //this.router.navigate(['/output']);
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.show('You are logged out', {
            cssClass: 'alert-success',
            timeout: 3000
        });
        this.router.navigate(['/login']);
        return false;
    };
    NavbarComponent.prototype.close = function () {
        var win = window.open("/", "_self"); /* url = “” or “about:blank”; target=”_self” */
        win.close();
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object])
    ], NavbarComponent);
    return NavbarComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/navbar.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/output/output.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".checkboxinsameline{\r\n    display:inline;\r\n}\r\n.right {\r\n    float:right;\r\n    width:100px;\r\n}\r\n.center {\r\n  float:right;\r\n  width:200px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/output/output.component.html":
/***/ (function(module, exports) {

module.exports = "<p>Hi <b>{{user}}</b> </p>\r\n\r\n<input type=\"hidden\" name=\"loggedmanager\" id=\"loggedmanager\"  value={{user}}/>\r\n\r\n  <p>Users assigned to you are : {{ primarymanagers ? primarymanagers.length: '0' }}</p>\r\n  <button [disabled]=\"! primarymanagers || primarymanagers.length==0\" class=\"btn btn-info\" id=\"submit\" (click)=saveResponse()>Submit</button>\r\n  <button [disabled]=\"! primarymanagers || primarymanagers.length==0\" class=\"btn btn-success\" id=\"clear\"(click)=clearSearch()>Clear</button>\r\n  <button [disabled]=\"! primarymanagers || primarymanagers.length==0\" (click)=\"exportToExcel()\" id=\"export\" class=\"btn btn-primary\">Export to excel</button>\r\n\r\n<div class=\"navbar-nav navbar-right\">\r\n<b> Mark All as  </b>\r\n    <div class=\"checkbox\" style=\"display: inline\" *ngFor=\"let status of statusAll\">\r\n       <label>\r\n         <input type=\"radio\" name=\"status\" (click)=\"setToRentControl(status.value)\" [(ngModel)]=\"radioValue\" [value]=\"status.value\">\r\n         <b>{{status.display}}</b>\r\n         </label>\r\n    </div>\r\n  </div>\r\n\r\n\r\n    <!-- <a class=\"btn btn-danger\" (click)=\"deleteApplication(primaryapplication._id)\">Delete</a> -->\r\n      <!-- <p>Filter List:  {{(primarymanagers|filterBy: userFilter).length}}</p> -->\r\n\r\n<table class=\"table table-bordered table-striped\">\r\n     <thead>\r\n     <tr>\r\n       <!-- <th>ID</th> -->\r\n       <th>Manager</th>\r\n       <th>Resource</th>\r\n       <!-- <th class=\"col-sm-1\">Initials</th> -->\r\n       <th>LogonID</th>\r\n         <!-- <th>User Location</th> -->\r\n       <th>Application</th>\r\n      <th>Status</th>\r\n     </tr>\r\n     </thead>\r\n     <tr>\r\n       <td><input type=\"text\" [(ngModel)]=\"userFilter.manager\" placeholder=\"Manager\"></td>\r\n       <td><input type=\"text\" [(ngModel)]=\"userFilter.resource\" placeholder=\"Resource\"></td>\r\n       <!-- <td class=\"col-sm-1\"><input type=\"text\" [(ngModel)]=\"userFilter.initials\" placeholder=\"Initials\"></td> -->\r\n       <td><input type=\"text\" [(ngModel)]=\"userFilter.logonid\" placeholder=\"LogonID\"></td>\r\n       <!--  <td><input type=\"text\" [(ngModel)]=\"userFilter.userlocation\" placeholder=\"User Location\"></td> -->\r\n       <td><input type=\"text\" [(ngModel)]=\"userFilter.application\" placeholder=\"Application\"></td>\r\n      <td><input type=\"text\" [(ngModel)]=\"userFilter.status\" placeholder=\"Status\"></td>\r\n\r\n    </tr>\r\n     <tbody>\r\n      <tr  *ngFor=\"let manager of primarymanagers  | filterBy: userFilter | paginate: {itemsPerPage: 10, currentPage:page, id: '1'}; let i = index\">\r\n      <!-- <td>{{manager._id}}</td> -->\r\n<!-- <div ng-repeat=\"manager in primarymanagers | filter:query as results\">\r\n<tr> -->\r\n       <td>{{manager.manager}}</td>\r\n       <td>{{manager.resource}}</td>\r\n       <!-- <td>{{manager.initials}}</td> -->\r\n       <td>{{manager.logonid}}</td>\r\n       <!-- <td>{{manager.userlocation}}</td> -->\r\n       <td>{{manager.application}}</td>\r\n       <td>\r\n\r\n       <select [(ngModel)]=\"manager.status\">\r\n           <option  *ngFor=\"let product of products\"\r\n             [value]=\"product.name\"\r\n             [attr.selected]=\"manager.status==product.name ? true : null\">\r\n           {{product.name}}\r\n           </option>\r\n         </select>\r\n  </td>\r\n\r\n      </tr>\r\n      <tr *ngIf=\"! primarymanagers || primarymanagers.length==0\">\r\n        <td colspan=\"6\" style=\"text-align: center;\">No Records exist</td>\r\n      </tr>\r\n     </tbody>\r\n\r\n   </table>\r\n<!-- <p>Showing {{filtered.length}} Records</p> -->\r\n\r\n\r\n   <div class=\"centre\">\r\n<pagination-controls class=\"pagination-lg\" (pageChange)=\"page = $event\" id=\"1\"\r\n             maxSize=\"10\"\r\n             directionLinks=\"true\"\r\n             autoHide=\"true\">\r\n       </pagination-controls>`\r\n</div>\r\n<div hidden>\r\n  <table class=\"table table-bordered table-striped\" id=\"filter_table_output\">\r\n    <thead>\r\n    <tr>\r\n      <th>Manager</th>\r\n      <th>Resource</th>\r\n      <th>LogonID</th>\r\n      <th>Application</th>\r\n     <th>Status</th>\r\n    </tr>\r\n    </thead>\r\n   \r\n    <tbody>\r\n     <tr  *ngFor=\"let manager of primarymanagers  | filterBy: userFilter ; track let i = index\" id={{i}}>\r\n     <td style=\"display: none;\">{{manager._id}}</td>\r\n      <td>{{manager.manager}}</td>\r\n      <td>{{manager.resource}}</td>\r\n      <td>{{manager.logonid}}</td>\r\n      <td>{{manager.application}}</td>\r\n      <td>\r\n\r\n      <select [(ngModel)]=\"manager.status\" id=\"{{'status-'+ i}}\">\r\n          <option  *ngFor=\"let product of products\"\r\n            [value]=\"product.name\"\r\n            [attr.selected]=\"manager.status==product.name ? true : null\">\r\n          {{product.name}}\r\n          </option>\r\n        </select>\r\n </td>\r\n\r\n     </tr>\r\n    </tbody>\r\n\r\n  </table>\r\n\r\n\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/output/output.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OutputComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_managerextract_service__ = __webpack_require__("../../../../../src/app/services/managerextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var OutputComponent = /** @class */ (function () {
    function OutputComponent(http, el, flashMessage, managerextractService, route, excelService, authService, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.managerextractService = managerextractService;
        this.route = route;
        this.excelService = excelService;
        this.authService = authService;
        this.router = router;
        this.statusOptions = [{ status: 'Not Recertified' }, { status: 'No Change' }, { status: 'Leaver' }, { status: 'Removal' }];
        this.userFilter = ({ manager: '' } || { resource: '' } || { initials: '' } || { userlocation: '' } || { application: '' } || { status: '' });
        this.Games = [{ name: "Not Recertified", type: "Not Recertified" },
            { name: "No Change", type: "No Change" },
            { name: "Leavers", type: "Leavers" },
            { name: "Removals", type: "Removals" }];
        //filter: ManagerExtract = new ManagerExtract();
        this.statusAll = [
            { value: 'NC', display: 'No Change' },
            { value: 'R', display: 'Removal' },
            { value: 'L', display: 'Leaver' },
            { value: 'NA', display: 'No Longer the Manager for User' }
        ];
        this.products = [
            { "id": 1, "name": "Not Recertified" },
            { "id": 2, "name": "No Change" },
            { "id": 3, "name": "Leaver" },
            { "id": 4, "name": "Removal" },
            //  { "id": 5, "name": "Moved to Special Cycle" },
            { "id": 6, "name": "No Longer the Manager for User" }
        ];
        this.selectedProduct = this.products[0];
        this.idList = [];
        this.id = this.route.snapshot.params['id'];
        /*getManagerExtractsClients(){
           console.log("this.user:getManagerExtractsClients:"+this.user);
            this.managerextractService.getManagerExtractsClients(this.user)
                .subscribe(primarymanagers=>{
                  this.primarymanagers = primarymanagers;
              });
          }*/
        this.items = ['Not Recertified', 'Leavers', 'Removals', 'No Change'];
        this.value = {};
        this._disabledV = '0';
        this.disabled = false;
        this.getManagerExtracts();
        //this.getManagerExtractsClients();
        this.excelService = excelService;
        // var url = document.location.href;
        // var temp = url.split('?');
        //temp = temp.split('='); // Now we have all the values in an array, with each element looking like "var=val";
        // for(var i=0; i<temp.length; i++)
        // {
        //   var qval2 = temp[i].split("=");
        //   var qval = qval2[i];
        //   qval = qval.replace("%40", "@");
        //     // console.log(qval);
        //      this.user = qval;
        //   //document.getElementById(qval[0]).value = qval[1];
        // }
        //console.log("this.user::"+this.user);
        //  this.games = [{"name":"Not Recertified","type":"Not Recertified"},
        //  {"name":"No Change","type":"No Change"},
        //  {"name":"Leavers","type":"Leavers"},
        //  {"name":"Removals","type":"Removals"}];
        this.user = this.authService.getUser();
    }
    OutputComponent.prototype.onSelect = function () {
        this.selectedProduct = null;
        //console.log(this.products.name);
        //for (var i = 0; i < this.products.length; i++)
        //{
        //if (this.products[i].id == productId) {
        //  this.selectedProduct = this.products[i];
        //}
        //}
    };
    OutputComponent.prototype.ngOnInit = function () {
        this.getManagerExtracts();
    };
    OutputComponent.prototype.setToRentControl = function (value) {
        var isList = [];
        this.idList = [];
        $("#filter_table_output tbody tr").each(function () {
            // isList.push(this.id);
            //  alert(this.name);
            //alert( $.trim($(this).find('td:eq(0)').html()));
            var managerId = $.trim($(this).find('td:eq(0)').html());
            // isList=[];
            if (value == 'R') {
                $('#status-' + this.id).val('Removal');
            }
            else if (value == 'NC') {
                $('#status-' + this.id).val('No Change');
            }
            else if (value == 'L') {
                $('#status-' + this.id).val('Leaver');
            }
            else if (value == 'NA') {
                $('#status-' + this.id).val('No Longer the Manager for User');
            }
            var data = {
                "id": managerId,
                "status": $('#status-' + this.id).val()
            };
            isList.push(data);
        });
        this.idList.push(isList);
        for (var i in this.primarymanagers) {
            for (var j in this.idList[0]) {
                if (this.idList[0][j].id == this.primarymanagers[i]._id) {
                    this.primarymanagers[i].status = this.idList[0][j].status; // here we are changing the status of the record   
                }
            }
        }
        // if(value=='R')
        // {
        //   for (let i in this.primarymanagers) {
        //     this.primarymanagers[i].status = 'Removal';
        //   }
        // }
        // else if(value=='NC')
        // {
        //   for (let i in this.primarymanagers) {
        //     this.primarymanagers[i].status = 'No Change';
        //   }
        // }
        // else if(value=='L')
        // {
        //   for (let i in this.primarymanagers) {
        //     this.primarymanagers[i].status = 'Leaver';
        //   }
        // }
        // else if(value=='NA')
        // {
        //   //alert("seletced Leavers");
        //   //this.products = [  { "id": 3, "name": "Leavers" }];
        //   for (let i in this.primarymanagers) {
        //     this.primarymanagers[i].status = 'No Longer the Manager for User';
        //   }
        // }
        // else
        // {
        //   //alert("seletced None");
        //   this.products =  [
        //         { "id": 1, "name": "Not Recertified" },
        //         { "id": 2, "name": "No Change" },
        //         { "id": 3, "name": "Leaver" },
        //         { "id": 4, "name": "Removal" },
        //         //{ "id": 5, "name": "Moved to Special Cycle" },
        //         { "id": 5, "name": "No Longer the Manager for User" }
        //       ];
        // }
    };
    OutputComponent.prototype.showSelectValue = function (mySelect) {
        console.log(mySelect);
    };
    OutputComponent.prototype.clearSearch = function () {
        window.location.reload();
    };
    //this.selectedProduct = this.route.snapshot.params['id'];
    //primarymanagersList : ({});
    OutputComponent.prototype.saveResponse = function () {
        var _this = this;
        console.log("in save response");
        console.log(this.primarymanagers);
        if (confirm("Are you sure want to submit responses ")) {
            console.log("Implement delete functionality here");
            //this.managerextractService.updateResponses(this.id,this.primarymanagers)
            //     .subscribe(()=> this.goBack());
            for (var i in this.primarymanagers) {
                for (var j in this.idList[0]) {
                    if (this.idList[0][j].id == this.primarymanagers[i]._id) {
                        var update = {
                            _id: this.primarymanagers[i]._id,
                            manager: this.primarymanagers[i].manager,
                            resource: this.primarymanagers[i].resource,
                            initials: this.primarymanagers[i].initials,
                            logonid: this.primarymanagers[i].logonid,
                            userlocation: this.primarymanagers[i].userlocation,
                            application: this.primarymanagers[i].application,
                            status: this.primarymanagers[i].status
                        };
                    }
                }
                //console.log("Length::"+i+"::"+update);
                if (update != null && update != undefined) {
                    this.managerextractService.updateResponses(update._id, update)
                        .subscribe(function () { return _this.goBack(); });
                }
            }
            this.flashMessage.show('Status submitted successfully', { cssClass: 'alert-success', timeout: 1000 });
            //this.router.navigate(['/output']);
            //window.location.reload();
        }
    };
    OutputComponent.prototype.goBack = function () {
        document.getElementById('submit').disabled = true;
        document.getElementById('clear').disabled = true;
        document.getElementById('export').disabled = true;
        window.setTimeout(this.refreshPage, 5000);
    };
    OutputComponent.prototype.refreshPage = function () {
        location.reload(true);
    };
    //goBack(){
    //  window.location.reload();
    //this.router.navigate(['output'])
    //this.router.navigate(['/output']);
    //window.location.reload();
    //}
    OutputComponent.prototype.getManagerExtracts = function () {
        var _this = this;
        console.log("this.user:getManagerExtracts:" + this.user);
        this.managerextractService.getManagerExtracts(this.user)
            .subscribe(function (primarymanagers) {
            _this.primarymanagers = primarymanagers;
        });
        /*this.managerextractService.getManagerExtracts()
            .subscribe(primarymanagers=>{
              this.primarymanagers = primarymanagers;
          }); */
    };
    Object.defineProperty(OutputComponent.prototype, "disabledV", {
        get: function () {
            return this._disabledV;
        },
        set: function (value) {
            this._disabledV = value;
            this.disabled = this._disabledV === '1';
        },
        enumerable: true,
        configurable: true
    });
    OutputComponent.prototype.selected = function (value) {
        console.log('Selected value is: ', value);
    };
    OutputComponent.prototype.removed = function (value) {
        console.log('Removed value is: ', value);
    };
    OutputComponent.prototype.typed = function (value) {
        console.log('New search input: ', value);
    };
    OutputComponent.prototype.refreshValue = function (value) {
        this.value = value;
    };
    OutputComponent.prototype.exportToExcel = function (event) {
        var json = [];
        for (var i in this.primarymanagers) {
            var update = {
                Manager: this.primarymanagers[i].manager,
                Resource: this.primarymanagers[i].resource,
                Logon_Id: this.primarymanagers[i].logonid,
                Region: this.primarymanagers[i].userlocation,
                Application: this.primarymanagers[i].application,
                Status: this.primarymanagers[i].status
            };
            json.push(update);
        }
        this.excelService.exportAsExcelFile(json, 'Primary_Cycle');
    };
    OutputComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-output',
            template: __webpack_require__("../../../../../src/app/components/output/output.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/output/output.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__services_managerextract_service__["a" /* ManagerextractService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_managerextract_service__["a" /* ManagerextractService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _h || Object])
    ], OutputComponent);
    return OutputComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/output.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationedit/primaryapplicationedit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationedit/primaryapplicationedit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Application Edit Form : You can edit an Applications's detail information into this SAM Apps.</div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\" (submit)=\"updatePrimaryApplications()\">\n<div class=\"form-group\">\n  <label for=\"mgr_name\" class=\"col-sm-2 control-label\">Application Name : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationName\" name=\"applicationName\" >\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"position\" class=\"col-sm-2 control-label\">Application Owner : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationOwner\" name=\"applicationOwner\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"department\" class=\"col-sm-2 control-label\">Business Unit : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.bu\" name=\"bu\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"salary\" class=\"col-sm-2 control-label\">Platform : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.platform\" name=\"platform\">\n  </div>\n</div>\n\n\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-2 col-sm-8\">\n    <button type=\"submit\" class=\"btn btn-success\">Update</button>\n     <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\n  </div>\n\n</div>\n</form>\n\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationedit/primaryapplicationedit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrimaryapplicationeditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__ = __webpack_require__("../../../../../src/app/services/primaryapplications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__primaryapplications__ = __webpack_require__("../../../../../src/app/components/primaryapplicationedit/primaryapplications.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PrimaryapplicationeditComponent = /** @class */ (function () {
    function PrimaryapplicationeditComponent(primaryapplicationsService, route, router) {
        this.primaryapplicationsService = primaryapplicationsService;
        this.route = route;
        this.router = router;
        this.primaryapplications = new __WEBPACK_IMPORTED_MODULE_3__primaryapplications__["a" /* Primaryapplications */]();
        this.id = this.route.snapshot.params['id'];
    }
    PrimaryapplicationeditComponent.prototype.ngOnInit = function () {
        this.getPrimaryApplication();
    };
    PrimaryapplicationeditComponent.prototype.getPrimaryApplication = function () {
        var _this = this;
        this.primaryapplicationsService.getPrimaryApplication(this.id)
            .subscribe(function (primaryapplications) {
            _this.primaryapplications = primaryapplications;
        });
    };
    PrimaryapplicationeditComponent.prototype.updatePrimaryApplications = function () {
        var _this = this;
        this.primaryapplicationsService.updatePrimaryApplications(this.id, this.primaryapplications)
            .subscribe(function () { return _this.goBack(); });
    };
    PrimaryapplicationeditComponent.prototype.goBack = function () {
        this.router.navigate(['/primaryapplications']);
    };
    PrimaryapplicationeditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-primaryapplicationedit',
            template: __webpack_require__("../../../../../src/app/components/primaryapplicationedit/primaryapplicationedit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/primaryapplicationedit/primaryapplicationedit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], PrimaryapplicationeditComponent);
    return PrimaryapplicationeditComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplicationedit.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationedit/primaryapplications.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Primaryapplications; });
var Primaryapplications = /** @class */ (function () {
    function Primaryapplications() {
    }
    return Primaryapplications;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplications.js.map

/***/ }),

/***/ "../../../../../src/app/components/primaryapplications/primaryapplications.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/primaryapplications/primaryapplications.component.html":
/***/ (function(module, exports) {

module.exports = "\n<a class=\"btn btn-info\" routerLink=\"/addprimaryapplications\">Add Application</a>\n <p>Total Applications: {{ primaryapplications ? primaryapplications.length: '0' }}</p>\n\n<table class=\"table table-bordered\">\n\t\t  <thead>\n\t\t\t<tr>\n\t\t\t  <td>Application Name</td>\n\t\t\t  <td>Application Owner</td>\n\t\t\t  <td>Business Unit</td>\n        <td>ExtractRecieved</td>\n\t\t\t  <td width=\"275\" align=\"center\">Action</td>\n\t\t\t</tr>\n\t\t  </thead>\n\t\t  <tbody>\n\t\t\t <tr  *ngFor=\"let primaryapplication of primaryapplications\">\n\t\t\t\t<td>{{primaryapplication.applicationName}}</td>\n\t\t\t\t<td>{{primaryapplication.applicationOwner}}</td>\n\t\t\t\t<td>{{primaryapplication.bu}}</td>\n        <td>{{primaryapplication.extractRecieved}}</td>\n\t\t\t\t<td width=\"275\">\n\t\t\t\t\t<a class=\"btn btn-info\" routerLink=\"/showprimaryapplications/{{primaryapplication._id}}\">Detail</a>\n\t\t\t\t\t<a class=\"btn btn-success\"  routerLink=\"/editprimaryapplications/{{primaryapplication._id}}\">Edit</a>\n\t\t\t\t\t<a class=\"btn btn-danger\" (click)=\"deletePrimaryApplications(primaryapplication._id)\">Delete</a>\n\t\t\t\t</td>\n\t\t\t </tr>\n\t\t\t <tr *ngIf=\"! primaryapplications\">\n\t\t\t\t<td colspan=\"6\" style=\"text-align: center;\">No Records exist</td>\n\t\t\t  </tr>\n\t\t  </tbody>\n\t\t</table>\n"

/***/ }),

/***/ "../../../../../src/app/components/primaryapplications/primaryapplications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrimaryapplicationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__ = __webpack_require__("../../../../../src/app/services/primaryapplications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PrimaryapplicationsComponent = /** @class */ (function () {
    function PrimaryapplicationsComponent(primaryapplicationsService, route, router) {
        this.primaryapplicationsService = primaryapplicationsService;
        this.route = route;
        this.router = router;
    }
    PrimaryapplicationsComponent.prototype.ngOnInit = function () {
        this.getPrimaryApplications();
    };
    PrimaryapplicationsComponent.prototype.getPrimaryApplications = function () {
        var _this = this;
        this.primaryapplicationsService.getPrimaryApplications()
            .subscribe(function (primaryapplications) {
            _this.primaryapplications = primaryapplications;
        });
    };
    PrimaryapplicationsComponent.prototype.deletePrimaryApplications = function (id) {
        var _this = this;
        this.primaryapplicationsService.deletePrimaryApplications(id)
            .subscribe(function () {
            _this.getPrimaryApplications();
        });
    };
    PrimaryapplicationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-primaryapplications',
            template: __webpack_require__("../../../../../src/app/components/primaryapplications/primaryapplications.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/primaryapplications/primaryapplications.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], PrimaryapplicationsComponent);
    return PrimaryapplicationsComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplications.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationsadd/primaryapplications.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Primaryapplications; });
var Primaryapplications = /** @class */ (function () {
    function Primaryapplications() {
    }
    return Primaryapplications;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplications.js.map

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationsadd/primaryapplicationsadd.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationsadd/primaryapplicationsadd.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\">\n<div class=\"panel-heading\">Application Add Entry Form </div>\n<div class=\"panel-body\">\n<form class=\"form-horizontal\" (submit)=\"addPrimaryApplications()\">\n<div class=\"form-group\">\n  <label for=\"mgr_name\" class=\"col-sm-2 control-label\">Application Name : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationName\" name=\"applicationName\" >\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"position\" class=\"col-sm-2 control-label\">Application Owner : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.applicationOwner\" name=\"applicationOwner\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"department\" class=\"col-sm-2 control-label\">Business Unit : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.bu\" name=\"bu\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <label for=\"salary\" class=\"col-sm-2 control-label\">Platform : </label>\n  <div class=\"col-sm-9\">\n    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"primaryapplications.platform\" name=\"platform\">\n  </div>\n</div>\n<div class=\"form-group\">\n  <div class=\"col-sm-offset-2 col-sm-8\">\n    <button type=\"submit\" class=\"btn btn-success\">Save</button>\n     <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\n  </div>\n\n</div>\n</form>\n\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationsadd/primaryapplicationsadd.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrimaryapplicationsaddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__ = __webpack_require__("../../../../../src/app/services/primaryapplications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__primaryapplications__ = __webpack_require__("../../../../../src/app/components/primaryapplicationsadd/primaryapplications.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PrimaryapplicationsaddComponent = /** @class */ (function () {
    function PrimaryapplicationsaddComponent(primaryapplicationsService, route, router) {
        this.primaryapplicationsService = primaryapplicationsService;
        this.route = route;
        this.router = router;
        this.primaryapplications = new __WEBPACK_IMPORTED_MODULE_3__primaryapplications__["a" /* Primaryapplications */]();
    }
    PrimaryapplicationsaddComponent.prototype.ngOnInit = function () {
    };
    PrimaryapplicationsaddComponent.prototype.addPrimaryApplications = function () {
        var _this = this;
        this.primaryapplicationsService.addPrimaryApplications(this.primaryapplications)
            .subscribe(function () { return _this.goBack(); });
    };
    PrimaryapplicationsaddComponent.prototype.goBack = function () {
        this.router.navigate(['/primaryapplications']);
    };
    PrimaryapplicationsaddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-primaryapplicationsadd',
            template: __webpack_require__("../../../../../src/app/components/primaryapplicationsadd/primaryapplicationsadd.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/primaryapplicationsadd/primaryapplicationsadd.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], PrimaryapplicationsaddComponent);
    return PrimaryapplicationsaddComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplicationsadd.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationshow/primaryapplicationshow.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationshow/primaryapplicationshow.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default\" >\r\n<div class=\"panel-heading\">Primary Cycle: Application Inscope</div>\r\n<div class=\"panel-body\">\r\n  <form class=\"form-horizontal\" *ngIf=primaryapplications>\r\n<div class=\"form-group\">\r\n  <label for=\"applicationName\" class=\"col-sm-2 control-label\">Application Name : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control\">{{primaryapplications.applicationName}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"applicationOwner\" class=\"col-sm-2 control-label\">Application Owner : </label>\r\n  <div class=\"col-sm-9\">\r\n   <p class=\"form-control\">{{primaryapplications.applicationOwner}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"bu\" class=\"col-sm-2 control-label\">Business Unit : </label>\r\n  <div class=\"col-sm-9\">\r\n   <p class=\"form-control\">{{primaryapplications.bu}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"platform\" class=\"col-sm-2 control-label\">Platform : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control\">{{primaryapplications.platform}}</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label for=\"id\" class=\"col-sm-2 control-label\">Applications's ID : </label>\r\n  <div class=\"col-sm-9\">\r\n    <p class=\"form-control\">08df088d89dsfhjf87</p>\r\n  </div>\r\n</div>\r\n<div class=\"form-group\">\r\n  <div class=\"col-sm-offset-2 col-sm-8\">\r\n\r\n    <a class=\"btn btn-info\" (click)=\"goBack()\">Cancel</a>\r\n  </div>\r\n\r\n</div>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/primaryapplicationshow/primaryapplicationshow.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrimaryapplicationshowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__ = __webpack_require__("../../../../../src/app/services/primaryapplications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PrimaryapplicationshowComponent = /** @class */ (function () {
    function PrimaryapplicationshowComponent(primaryapplicationsService, route, router) {
        this.primaryapplicationsService = primaryapplicationsService;
        this.route = route;
        this.router = router;
    }
    PrimaryapplicationshowComponent.prototype.ngOnInit = function () {
        this.getPrimaryApplication();
    };
    PrimaryapplicationshowComponent.prototype.getPrimaryApplication = function () {
        var _this = this;
        var id = this.route.snapshot.params['id'];
        this.primaryapplicationsService.getPrimaryApplication(id)
            .subscribe(function (primaryapplications) {
            _this.primaryapplications = primaryapplications;
        });
    };
    PrimaryapplicationshowComponent.prototype.goBack = function () {
        this.router.navigate(['/primaryapplications']);
    };
    PrimaryapplicationshowComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-primaryapplicationshow',
            template: __webpack_require__("../../../../../src/app/components/primaryapplicationshow/primaryapplicationshow.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/primaryapplicationshow/primaryapplicationshow.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_primaryapplications_service__["a" /* PrimaryapplicationsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], PrimaryapplicationshowComponent);
    return PrimaryapplicationshowComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplicationshow.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\">\n  <h2 class=\"page-header\">{{user.name}}</h2>\n  <ul class=\"list-group\">\n    <li class=\"list-group-item\">Username: {{user.username}}</li>\n    <li class=\"list-group-item\">Email: {{user.email}}</li>\n  </ul>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            console.log("Profile::" + profile);
            _this.user = profile.user;
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("../../../../../src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object])
    ], ProfileComponent);
    return ProfileComponent;
    var _a, _b;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/profile.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "form {\r\n    width: 300px;\r\n    margin: 0 auto;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "\n<form (submit)=\"onRegisterSubmit()\">\n  <h2 class=\"page-header\">Register</h2>\n  <div class=\"form-group\">\n    <label>Name</label>\n    <input style='display:block;width:auto' type=\"text\" [(ngModel)]=\"name\" name=\"name\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Username</label>\n    <input style='display:block;width:auto' type=\"text\" [(ngModel)]=\"username\" name=\"username\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <label>Email</label>\n    <input style='display:block;width:auto' type=\"text\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\" >\n  </div>\n  <div class=\"form-group\">\n    <label>Password</label>\n    <input style='display:block;width:auto' type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\">\n  </div>\n  <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(validateService, flashMessage, authService, router) {
        this.validateService = validateService;
        this.flashMessage = flashMessage;
        this.authService = authService;
        this.router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password
        };
        // Required Fields
        if (!this.validateService.validateRegister(user)) {
            this.flashMessage.show('Please fill in all fields', { cssClass: 'alert-danger', timeout: 3000 });
            return false;
        }
        // Validate Email
        if (!this.validateService.validateEmail(user.email)) {
            this.flashMessage.show('Please use a valid email', { cssClass: 'alert-danger', timeout: 3000 });
            return false;
        }
        // Register user
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                _this.flashMessage.show('You are now registered and can log in', { cssClass: 'alert-success', timeout: 3000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
                _this.router.navigate(['/register']);
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */]) === "function" && _d || Object])
    ], RegisterComponent);
    return RegisterComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/register.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/userextract/userextract.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/userextract/userextract.component.html":
/***/ (function(module, exports) {

module.exports = "<h4>\r\n<!-- here we echo the title from the component -->\r\n  <b>{{title}}</b>\r\n</h4>\r\n<input type=\"file\" name=\"photo\" ng2FileSelect [uploader]=\"uploader\" multiple  accept=\".xlsx, .xls\"/>\r\n   <button type=\"button\" class=\"btn btn-success btn-s\" [disabled]=\"!uploader.getNotUploadedItems().length\"\r\n                 (click)=\"uploader.uploadAll();\">\r\n           <span class=\"glyphicon glyphicon-upload\"></span> Upload users\r\n         </button>\r\n      <!--   <a class=\"btn btn-light\"  (click)=\"download()\">Download Report</a>  -->\r\n      <button (click)=\"exportToExcel()\" class=\"btn btn-primary\">Export to excel</button>\r\n      <br />\r\n <p>Total Number of users uploaded:<b> {{ primaryusers ? primaryusers.length: '0' }} </b></p>\r\n       <table class=\"table table-bordered table-striped\">\r\n         \t\t  <thead>\r\n         \t\t\t<tr>\r\n         \t\t\t  <th>LogonID </th>\r\n         \t\t\t  <th>Application</th>\r\n         \t\t\t  <th>Region</th>\r\n                <th>UserName </th>\r\n         \t\t\t</tr>\r\n         \t\t  </thead>\r\n         \t\t  <tbody>\r\n              <tr  *ngFor=\"let primaryuser of primaryusers | paginate: {itemsPerPage: 10, currentPage:page, id: '1'}; let i = index\">\r\n         \t\t\t\t<td>{{primaryuser.logonid}}</td>\r\n         \t\t\t\t<td>{{primaryuser.application}}</td>\r\n         \t\t\t\t<td>{{primaryuser.region}}</td>\r\n                <td>{{primaryuser.name}}</td>\r\n         \t\t\t </tr>\r\n         \t\t  </tbody>\r\n         \t\t</table>\r\n            <div class=\"centre\">\r\n    <pagination-controls class=\"pagination-lg\" (pageChange)=\"page = $event\" id=\"1\"\r\n                      maxSize=\"10\"\r\n                      directionLinks=\"true\"\r\n                      autoHide=\"true\">\r\n                </pagination-controls>`\r\n   </div>\r\n\r\n  <!-- <div id=\"report1\" >\r\n     <table class=\"table table-bordered table-striped\" >\r\n           <thead>\r\n           <tr>\r\n             <th>LogonID </th>\r\n             <th>Application</th>\r\n             <th>Region</th>\r\n             <th>UserName </th>\r\n           </tr>\r\n           </thead>\r\n           <tbody>\r\n             <tr  *ngFor=\"let primaryuser of primaryusers\">\r\n              <td>{{primaryuser.logonid}}</td>\r\n              <td>{{primaryuser.application}}</td>\r\n              <td>{{primaryuser.region}}</td>\r\n              <td>{{primaryuser.name}}</td>\r\n            </tr>\r\n           </tbody>\r\n         </table>\r\n     </div> -->\r\n"

/***/ }),

/***/ "../../../../../src/app/components/userextract/userextract.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserextractComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_userextract_service__ = __webpack_require__("../../../../../src/app/services/userextract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_excel_service__ = __webpack_require__("../../../../../src/app/services/excel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var URL = 'http://localhost:8080/api/userextract';
var UserextractComponent = /** @class */ (function () {
    function UserextractComponent(http, el, flashMessage, userextractService, excelService, authuser, router) {
        this.http = http;
        this.el = el;
        this.flashMessage = flashMessage;
        this.userextractService = userextractService;
        this.excelService = excelService;
        this.authuser = authuser;
        this.router = router;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: URL, itemAlias: 'photo' });
        this.title = 'Application Users';
        this.getUsers();
        this.excelService = excelService;
        // this.persons = PERSONS;
        //this.primaryusers = primaryusers;
    }
    //persons: Person[];
    UserextractComponent.prototype.ngOnInit = function () {
        var _this = this;
        //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
        this.uploader.onAfterAddingFile = function (file) { file.withCredentials = false; };
        //overide the onCompleteItem property of the uploader so we are
        //able to deal with the server response.
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            console.log("ImageUpload:uploaded:", item, status, response);
            var responsePath = JSON.parse(response);
            console.log("ImageUpload--> completion:", response, responsePath); // the url will be in the response
            _this.flashMessage.show('Files uploaded successfully', { cssClass: 'alert-success', timeout: 3000 });
            _this.router.navigate(['/userextracts']);
            //window.location.reload();
        };
        this.uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
            window.location.reload();
        };
    };
    //the function which handles the file upload without using a plugin.
    UserextractComponent.prototype.upload = function () {
        //locate the file element meant for the file upload.
        var inputEl = this.el.nativeElement.querySelector('#photo');
        //get the total amount of files attached to the file input.
        var fileCount = inputEl.files.length;
        //create a new fromdata instance
        var formData = new FormData();
        //check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) {
            //append the key name 'photo' with the first file in the element
            formData.append('photo', inputEl.files.item(0));
            //call the angular http method
            this.http
                .post(URL, formData).map(function (res) { return res.json(); }).subscribe(
            //map the success function and alert the response
            function (success) {
                alert(success._body);
            }, function (error) { return alert(error); });
        }
    };
    UserextractComponent.prototype.getUsers = function () {
        var _this = this;
        this.userextractService.getUsers()
            .subscribe(function (primaryusers) {
            _this.primaryusers = primaryusers;
        });
    };
    UserextractComponent.prototype.download = function () {
        var htmltable = document.getElementById('report1');
        var html = htmltable.innerHTML;
        window.open('data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html));
        console.log("download button clicked");
    };
    UserextractComponent.prototype.exportToExcel = function (event) {
        this.excelService.exportAsExcelFile(this.primaryusers, 'primaryusers');
    };
    UserextractComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-userextract',
            template: __webpack_require__("../../../../../src/app/components/userextract/userextract.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/userextract/userextract.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__["FlashMessagesService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__["FlashMessagesService"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__services_userextract_service__["a" /* UserextractService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_userextract_service__["a" /* UserextractService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_excel_service__["a" /* ExcelService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_8__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__services_auth_service__["a" /* AuthService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _g || Object])
    ], UserextractComponent);
    return UserextractComponent;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/userextract.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/userupload/userupload.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/userupload/userupload.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"file\" class=\"form-control\" name=\"single\" ng2FileSelect [uploader]=\"uploader\"  accept=\".xlsx, .xls/>\r\n   <button type=\"button\" class=\"btn btn-success btn-s\"\r\n                 (click)=\"uploader.uploadAll();\">\r\n           <span class=\"glyphicon glyphicon-upload\"></span> Upload all\r\n         </button><br />\r\n <p>Total Number of users uploaded: {{ primaryusers ? primaryusers.length: '0' }}</p>\r\n\r\n       <table class=\"table table-bordered\">\r\n         \t\t  <thead>\r\n         \t\t\t<tr>\r\n         \t\t\t  <td>LogonID </td>\r\n         \t\t\t  <td>Application</td>\r\n         \t\t\t  <td>Region</td>\r\n                <td>Name </td>\r\n\r\n         \t\t\t</tr>\r\n         \t\t  </thead>\r\n         \t\t  <tbody>\r\n         \t\t\t <tr  *ngFor=\"let primaryuser of primaryusers\">\r\n         \t\t\t\t<td>{{primaryuser.logonid}}</td>\r\n         \t\t\t\t<td>{{primaryuser.application}}</td>\r\n         \t\t\t\t<td>{{primaryuser.region}}</td>\r\n                <td>{{primaryuser.name}}</td>\r\n\r\n         \t\t\t </tr>\r\n         \t\t  </tbody>\r\n         \t\t</table>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/userupload/userupload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UseruploadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_uploaduser_service__ = __webpack_require__("../../../../../src/app/services/uploaduser.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UseruploadComponent = /** @class */ (function () {
    function UseruploadComponent(uploaduserService, route, router) {
        this.uploaduserService = uploaduserService;
        this.route = route;
        this.router = router;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: "http://localhost:8080/api/uploaduser/" });
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            var responsePath = JSON.parse(response);
            console.log(response, responsePath); // the url will be in the response
        };
    }
    UseruploadComponent.prototype.ngOnInit = function () {
        this.getUploadusers();
    };
    UseruploadComponent.prototype.getUploadusers = function () {
        var _this = this;
        this.uploaduserService.getUploadusers()
            .subscribe(function (primaryusers) {
            _this.primaryusers = primaryusers;
            console.log(_this.primaryusers);
            //this.goBack();
            _this.router.navigate(['/uploaduser']);
        });
    };
    UseruploadComponent.prototype.goBack = function () {
        this.router.navigate(['/uploaduser']);
    };
    UseruploadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-userupload',
            template: __webpack_require__("../../../../../src/app/components/userupload/userupload.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/userupload/userupload.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_uploaduser_service__["a" /* UploaduserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_uploaduser_service__["a" /* UploaduserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object])
    ], UseruploadComponent);
    return UseruploadComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/userupload.component.js.map

/***/ }),

/***/ "../../../../../src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object])
    ], AuthGuard);
    return AuthGuard;
    var _a, _b;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/auth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/models/applications.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Applications; });
var Applications = /** @class */ (function () {
    function Applications() {
    }
    return Applications;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/applications.js.map

/***/ }),

/***/ "../../../../../src/app/models/managerchange.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerChange; });
var ManagerChange = /** @class */ (function () {
    function ManagerChange() {
    }
    return ManagerChange;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerchange.js.map

/***/ }),

/***/ "../../../../../src/app/services/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        console.log('PostService Initialized...');
    }
    ApiService.prototype.getApplications = function () {
        return this.http.get("http://localhost:8085/api/upload")
            .map(function (res) { return res.json(); });
    };
    ApiService.prototype.deleteApplication = function (id) {
        return this.http.delete("http://localhost:8085/api/upload/" + id)
            .map(function (res) { return res.json(); });
    };
    ApiService.prototype.getApplication = function (id) {
        return this.http.get("http://localhost:8085/api/upload/" + id)
            .map(function (res) { return res.json(); });
    };
    ApiService.prototype.updateApplication = function (id, info) {
        return this.http.put("http://localhost:8085/api/upload/" + id, info)
            .map(function (res) { return res.json(); });
    };
    ApiService.prototype.addApplication = function (info) {
        return this.http.post("http://localhost:8085/api/uploadapplication", info)
            .map(function (res) { return res.json(); });
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === "function" && _a || Object])
    ], ApiService);
    return ApiService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/api.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.isDev = true; // Change to false before deployment
    }
    AuthService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var ep = this.prepEndpoint('users/register');
        return this.http.post(ep, user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.authenticateUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var ep = this.prepEndpoint('users/authenticate');
        return this.http.post(ep, user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.getProfile = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        var ep = this.prepEndpoint('users/profile');
        return this.http.get(ep, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.storeUserData = function (token, user, username) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('username', username);
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authToken = token;
    };
    AuthService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["tokenNotExpired"])();
    };
    AuthService.prototype.getUser = function () {
        var userData = localStorage.getItem('username');
        return userData;
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.prepEndpoint = function (ep) {
        if (this.isDev) {
            return ep;
        }
        else {
            return 'http://localhost:8085/' + ep;
        }
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], AuthService);
    return AuthService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/changemanager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangemanagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChangemanagerService = /** @class */ (function () {
    function ChangemanagerService(http) {
        this.http = http;
    }
    ChangemanagerService.prototype.getManagers = function () {
        return this.http.get("http://localhost:8085/api/changemanager")
            .map(function (res) { return res.json(); });
    };
    ChangemanagerService.prototype.updateManager = function (id, info) {
        return this.http.put("http://localhost:8085/api/changemanager/" + id, info)
            .map(function (res) { return res.json(); });
    };
    ChangemanagerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], ChangemanagerService);
    return ChangemanagerService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/changemanager.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/dashboard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardService = /** @class */ (function () {
    function DashboardService(http) {
        this.http = http;
    }
    DashboardService.prototype.getUsersDashboard = function () {
        return this.http.get("http://localhost:8085/api/dashboard")
            .map(function (res) { return res.json(); });
    };
    DashboardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], DashboardService);
    return DashboardService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/dashboard.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/excel.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExcelService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_file_saver__ = __webpack_require__("../../../../file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx__ = __webpack_require__("../../../../xlsx/xlsx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_xlsx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExcelService = /** @class */ (function () {
    function ExcelService() {
    }
    ExcelService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = __WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        var excelBuffer = __WEBPACK_IMPORTED_MODULE_2_xlsx__["write"](workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExcelService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        __WEBPACK_IMPORTED_MODULE_1_file_saver__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExcelService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ExcelService);
    return ExcelService;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/excel.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pager_service__ = __webpack_require__("../../../../../src/app/services/pager.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__pager_service__["a"]; });

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/index.js.map

/***/ }),

/***/ "../../../../../src/app/services/managerextract.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerextractService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ManagerextractService = /** @class */ (function () {
    function ManagerextractService(http) {
        this.http = http;
    }
    /*  getManagerExtractsClients(manager){
        console.log("from service::"+manager);
          return this.http.get("http://localhost:8085/api/managerextract/"+manager)
              .map(res => res.json());
        } */
    ManagerextractService.prototype.getManagerExtracts = function (manager) {
        console.log("from service::" + manager);
        return this.http.get("http://localhost:8085/api/managerextract/" + manager)
            .map(function (res) { return res.json(); });
    };
    /*  getManagerExtractsClients(id){
        //console.log("this.user:getManagerExtracts service:"+id);
            return this.http.get("http://localhost:8085/api/managerextract")
                .map(res => res.json());
          }
  */
    ManagerextractService.prototype.getManagers = function () {
        return this.http.get("http://localhost:8085/api/managerchange")
            .map(function (res) { return res.json(); });
    };
    ManagerextractService.prototype.getManager = function (id) {
        return this.http.get("http://localhost:8085/api/managerchange/" + id)
            .map(function (res) { return res.json(); });
    };
    ManagerextractService.prototype.updateManager = function (id, info) {
        return this.http.put("http://localhost:8085/api/managerchange/" + id, info)
            .map(function (res) { return res.json(); });
    };
    ManagerextractService.prototype.updateResponses = function (id, info) {
        console.log("in service function of update response");
        console.log("in service function of update response" + id, info);
        return this.http.post("http://localhost:8085/api/manageroutput/" + id, info)
            .map(function (res) { return res.json(); });
    };
    ManagerextractService.prototype.delegateManagers = function (oldmanager, newmanager) {
        console.log("in service function of delegateManager" + oldmanager);
        console.log("in service function of delegateManager" + newmanager);
        var manager = oldmanager + "||" + newmanager;
        console.log("in service function of manager" + manager);
        return this.http.put("http://localhost:8085/api/delegatemanager/" + manager, newmanager)
            .map(function (res) { return res.json(); });
    };
    ManagerextractService.prototype.deleteDelegateManagers = function (manager) {
        console.log("in service function of manager" + manager);
        return this.http.delete("http://localhost:8085/api/delegatemanager/" + manager)
            .map(function (res) { return res.json(); });
    };
    ManagerextractService.prototype.getDelegateManager = function (manager) {
        return this.http.get("http://localhost:8085/api/delegatemanager/" + manager)
            .map(function (res) { return res.json(); });
    };
    ManagerextractService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], ManagerextractService);
    return ManagerextractService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/managerextract.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/mlogin.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MloginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MloginService = /** @class */ (function () {
    function MloginService(http) {
        this.http = http;
    }
    MloginService.prototype.authenticateManager = function (info) {
        return this.http.get("http://localhost:8085/api/mlogin", info)
            .map(function (res) { return res.json(); });
    };
    MloginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], MloginService);
    return MloginService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/mlogin.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/pager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("../../../../underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagerService = /** @class */ (function () {
    function PagerService() {
    }
    PagerService.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        // calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);
        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        }
        else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            }
            else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            }
            else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        // create an array of pages to ng-repeat in the pager control
        var pages = __WEBPACK_IMPORTED_MODULE_0_underscore__["range"](startPage, endPage + 1);
        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    };
    PagerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], PagerService);
    return PagerService;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/pager.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/primaryapplications.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrimaryapplicationsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PrimaryapplicationsService = /** @class */ (function () {
    function PrimaryapplicationsService(http) {
        this.http = http;
    }
    PrimaryapplicationsService.prototype.getPrimaryApplications = function () {
        return this.http.get("http://localhost:8085/api/primaryapplications")
            .map(function (res) { return res.json(); });
    };
    PrimaryapplicationsService.prototype.addPrimaryApplications = function (info) {
        return this.http.post("http://localhost:8085/api/primaryapplications", info)
            .map(function (res) { return res.json(); });
    };
    PrimaryapplicationsService.prototype.getPrimaryApplication = function (id) {
        return this.http.get("http://localhost:8085/api/primaryapplications/" + id)
            .map(function (res) { return res.json(); });
    };
    PrimaryapplicationsService.prototype.deletePrimaryApplications = function (id) {
        return this.http.delete("http://localhost:8085/api/primaryapplications/" + id)
            .map(function (res) { return res.json(); });
    };
    PrimaryapplicationsService.prototype.updatePrimaryApplications = function (id, info) {
        return this.http.put("http://localhost:8085/api/primaryapplications/" + id, info)
            .map(function (res) { return res.json(); });
    };
    PrimaryapplicationsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], PrimaryapplicationsService);
    return PrimaryapplicationsService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/primaryapplications.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/searchfilter.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchFilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchFilterPipe = /** @class */ (function () {
    function SearchFilterPipe() {
    }
    SearchFilterPipe.prototype.transform = function (items, field, value) {
        if (!items)
            return [];
        return items.filter(function (it) { return it[field] == value; });
    };
    SearchFilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'searchfilter'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], SearchFilterPipe);
    return SearchFilterPipe;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/searchfilter.js.map

/***/ }),

/***/ "../../../../../src/app/services/uploadmanager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadmanagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UploadmanagerService = /** @class */ (function () {
    function UploadmanagerService(http) {
        this.http = http;
    }
    UploadmanagerService.prototype.getUploadmanagers = function () {
        return this.http.get("http://localhost:8085/api/uploadmanager")
            .map(function (res) { return res.json(); });
    };
    UploadmanagerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], UploadmanagerService);
    return UploadmanagerService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/uploadmanager.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/uploaduser.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploaduserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UploaduserService = /** @class */ (function () {
    function UploaduserService(http) {
        this.http = http;
    }
    UploaduserService.prototype.getUploadusers = function () {
        return this.http.get("http://localhost:8080/api/uploaduser")
            .map(function (res) { return res.json(); });
    };
    UploaduserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], UploaduserService);
    return UploaduserService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/uploaduser.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/userextract.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserextractService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserextractService = /** @class */ (function () {
    function UserextractService(http) {
        this.http = http;
    }
    UserextractService.prototype.getUsers = function () {
        return this.http.get("http://localhost:8085/api/userextract")
            .map(function (res) { return res.json(); });
    };
    UserextractService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
    ], UserextractService);
    return UserextractService;
    var _a;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/userextract.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidateService = /** @class */ (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegister = function (user) {
        if (user.name == undefined || user.email == undefined || user.username == undefined || user.password == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    ValidateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());

//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/validate.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=C:/Users/sgorlagunta/SAM/client/angular-src/src/main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map