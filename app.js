const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const session = require('express-session');

const fileUpload = require('express-fileupload');
var multer = require('multer');

// Connect To Database
mongoose.connect(config.database);

// On Connection
mongoose.connection.on('connected', () => {
  console.log('Connected to database '+config.database);
});

// On Error
mongoose.connection.on('error', (err) => {
  console.log('Database error: '+err);
});

const app = express();

// routers
const users = require('./routes/users');
const primaryapplications = require('./routes/primaryapplications');
const uploaduser = require('./routes/uploadusers');
const uploadmanager = require('./routes/uploadmanagers');
const changemanager = require('./routes/changemanager');
const mlogin = require('./routes/mlogin');
const application = require('./routes/application');
const applicationadd = require('./routes/applicationadd');
const userextract = require('./routes/userextract');
//const managerextract = require('./routes/managerextract');
const managerchange = require('./routes/managerchange');
const manageroutput = require('./routes/manageroutput');
const dashboard = require('./routes/dashboard');
//const managerextractclient = require('./routes/managerextractclient');
const managerextractclient = require('./routes/managerextractclient');
const delegatemanager = require('./routes/delegatemanager');



// Port Number
const port = process.env.PORT || 8085;

// CORS Middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);
app.use('/api/primaryapplications',primaryapplications);
app.use('/api/uploaduser',uploaduser);
app.use('/api/uploadmanager',uploadmanager);
app.use('/api/changemanager',changemanager);
app.use('/api/mlogin',mlogin);
app.use('/api/upload',application);
app.use('/api/uploadapplication',applicationadd);
app.use('/api/userextract',userextract);
//app.use('/api/managerextract',managerextract);
app.use('/api/managerchange',managerchange);
app.use('/api/manageroutput',manageroutput);
app.use('/api/dashboard',dashboard);
app.use('/api/managerextract',managerextractclient);
app.use('/api/delegatemanager',delegatemanager);








// Index Route
app.get('/', (req, res) => {
  res.send('Invalid Endpoint');
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

// Start Server
app.listen(port, () => {
  console.log('Server started on port '+port);
});
