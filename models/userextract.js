const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const UserExtractsSchema = mongoose.Schema({
  logonid: {
    type: String
  },
  application: {
    type: String
  },
  region: {
    type: String
  },
  name: {
    type: String
  }},
  { versionKey: false }
  );


const UserExtract = module.exports = mongoose.model('userextract', UserExtractsSchema);

module.exports.getUsers = function(callback){
    UserExtract.find({ }, { logonid: 1, application: 1, region: 1, name: 1, _id: 0 },callback);
}
module.exports.addUsers = function(newUploadusers, callback){
    UserExtract.create(newUploadusers, callback);
}
module.exports.updateUsers = function(id, newUploadusers, callback){
    UserExtract.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.deleteUsers = function(id, callback){
    UserExtract.findByIdAndRemove(id, callback);
}
module.exports.getUser = function(id, callback){
    UserExtract.findById(id, callback);
}
