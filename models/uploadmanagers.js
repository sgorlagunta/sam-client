const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const UploadManagersSchema = mongoose.Schema({
  manager: {
    type: String
  },
  resorce: {
    type: String
  },
  initials: {
    type: String
  },
  logonid: {
    type: String
  },
  userlocation: {
    type: String
  },
  application: {
    type: String
  },
  status: {
    type: String
  },
  comments: {
    type: String
  }
  },
    { versionKey: false });


const UploadManagers = module.exports = mongoose.model('uploadmanagers', UploadManagersSchema);

module.exports.getUploadmanagers = function(callback){
    UploadManagers.find(callback);
}
module.exports.addUploadmanagers = function(newUploadmanagers, callback){
    UploadManagers.create(newUploadmanagers, callback);
}
module.exports.updateUploadmanagers = function(id, newUploadmanagers, callback){
    UploadManagers.findByIdAndUpdate(id, newUploadmanagers, callback);
}
module.exports.deleteUploadmanagers = function(id, callback){
    UploadUsers.findByIdAndRemove(id, callback);
}
module.exports.getUploadmanager = function(id, callback){
    UploadManagers.findById(id, callback);
}
module.exports.getManagers = function(callback){
    UploadManagers.distinct('manager',callback);
}
module.exports.getManagerByUsername = function(manager,callback){
    UploadManagers.findOne({'manager':manager},{manager:1},callback);
}
