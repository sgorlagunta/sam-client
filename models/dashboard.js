const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const DashboardExtractsSchema = mongoose.Schema({
  manager: {
    type: String
  },
  resource: {
    type: String
  },
  initials: {
    type: String
  },
  logonid: {
    type: String
  },
  userlocation: {
    type: String
  },
  application: {
    type: String
  },
  status: {
    type: String
  },
  comments: {
    type: String
  }
  },
    { versionKey: false });

const DashboardExtract = module.exports = mongoose.model('dashboard', DashboardExtractsSchema);

module.exports.getUsersDashboard = function(callback){
    DashboardExtract.find(callback);
}
