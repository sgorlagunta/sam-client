const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const PrimaryApplicationsSchema = mongoose.Schema({
  applicationName: {
    type: String
  },
  applicationOwner: {
    type: String
  },
  bu: {
    type: String
  },
  platform: {
    type: String
  },
  classification: {
    type: String
  },
  scope: {
    type: String
  },
  extractRecieved: {
    type: String
  }
});

const PrimaryApplications = module.exports = mongoose.model('PrimaryApplications', PrimaryApplicationsSchema);

module.exports.getPrimaryApplications = function(callback){
    PrimaryApplications.find(callback);
}
module.exports.addPrimaryApplications = function(newPrimaryApplications, callback){
    PrimaryApplications.create(newPrimaryApplications, callback);
}
module.exports.updatePrimaryApplications = function(id, newPrimaryApplications, callback){
    PrimaryApplications.findByIdAndUpdate(id, newPrimaryApplications, callback);
}
module.exports.deletePrimaryApplications = function(id, callback){
    PrimaryApplications.findByIdAndRemove(id, callback);
}
module.exports.getPrimaryApplication = function(id, callback){
    PrimaryApplications.findById(id, callback);
}
