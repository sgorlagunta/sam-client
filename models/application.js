const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const ApplicationsSchema = mongoose.Schema({
  applicationName: {
    type: String
  },
  applicationOwner: {
    type: String
  },
  bu: {
    type: String
  },
  platform: {
    type: String
  },
  classification: {
    type: String
  },
  scope: {
    type: String
  },
  poc: {
    type: String
  },
  comments: {
    type: String
  }
},
  { versionKey: false });

const Applications = module.exports = mongoose.model('Applications', ApplicationsSchema);

module.exports.getApplications = function(callback){
    Applications.find(callback);
    //Applications.find({ }, { _id: 0 },callback);
        //UserExtract.find({ }, { logonid: 1, application: 1, region: 1, name: 1, _id: 0 },callback);
}
module.exports.addApplications = function(newApplications, callback){
    Applications.create(newApplications, callback);
}
module.exports.addApplication = function(newApplications, callback){
    Applications.create(newApplications, callback);
}
module.exports.updateApplication = function(id, newApplications, callback){
    Applications.findByIdAndUpdate(id, newApplications, callback);
}
module.exports.deleteApplication = function(id, callback){
    Applications.findByIdAndRemove(id, callback);
}
module.exports.getApplication = function(id, callback){
    Applications.findById(id, callback);
}
