const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const ManagerExtractsSchema = mongoose.Schema({
  manager: {
    type: String
  },
  resource: {
    type: String
  },
  initials: {
    type: String
  },
  logonid: {
    type: String
  },
  userlocation: {
    type: String
  },
  application: {
    type: String
  },
  status: {
    type: String
  },
  dmanager: {
  type: String
  },
  comments: {
    type: String
  }
  },
    { versionKey: false });


const ManagerExtract = module.exports = mongoose.model('managerextracts', ManagerExtractsSchema);

module.exports.getManagerExtracts = function(callback){
    ManagerExtract.find({ }, { },callback);
        //UserExtract.find({ }, { logonid: 1, application: 1, region: 1, name: 1, _id: 0 },callback);
}

module.exports.getManagerExtractsClients = function(manager,callback){
    //ManagerExtract.find( {'manager':manager},callback);
    //ManagerExtract.find( { '$or': [ {'manager':manager}, {'dmanager':manager} ] },callback);

    ManagerExtract.find({'status':{$nin : ['Moved to Special Cycle','No Change','Leaver','Removal'] },'$or': [ {'manager':manager}, {'dmanager':manager} ] },callback);
        //UserExtract.find({ }, { logonid: 1, application: 1, region: 1, name: 1, _id: 0 },callback);
}

module.exports.addManagers = function(newUploadusers, callback){
    ManagerExtract.create(newUploadusers, callback);
}
module.exports.updateManager = function(id, newUploadusers, callback){
    ManagerExtract.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.deleteManager = function(id, callback){
    ManagerExtract.findByIdAndRemove(id, callback);
}
module.exports.getManager = function(id, callback){
    ManagerExtract.findById(id, callback);
}
module.exports.getManagers = function(callback){
    ManagerExtract.distinct('manager',callback);
}
module.exports.getManagersName = function(manager,callback){
    ManagerExtract.find({'manager':manager},callback);
}
module.exports.updateManagerName = function(id, newUploadusers, callback){
    ManagerExtract.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.updateResponses = function(id, newApplications, callback){
    ManagerExtract.findByIdAndUpdate(id, newApplications, callback);
}

module.exports.getUsersDashboard = function(callback){
    ManagerExtract.find(callback);

    //ManagerExtract.distinct('application',callback);
}
module.exports.delegateManager = function(oldmanager, newmanager, callback){
  console.log("old manager::"+oldmanager);
  console.log("old newmanager::"+newmanager);
  //ManagerExtract.updateMany({'manager':oldmanager},{$set:{'dmanager':newmanager}},callback);
  ManagerExtract.update({"manager":oldmanager},{$set:{"dmanager":newmanager}},{multi:true},callback);
      //ManagerExtract.updateMany({'manager':oldmanager},callback);
}

module.exports.getUserByUsername = function(manager, callback){
    ManagerExtract.findOne({ '$or': [ {'manager':manager}, {'dmanager':manager} ] },callback);
}

module.exports.getDelegateManager = function(manager,callback){
    //ManagerExtract.find({'manager':manager},callback);
    ManagerExtract.findOne({'manager':manager}, { manager: 1, dmanager: 1},callback);
}

module.exports.deleteDelegateManager = function(manager, callback){
  console.log("manager::"+manager);
    //ManagerExtract.updateMany({'manager':oldmanager},{$set:{'dmanager':newmanager}},callback);
  ManagerExtract.update({"manager":manager},{$set:{"dmanager":""}},{multi:true},callback);
      //ManagerExtract.updateMany({'manager':oldmanager},callback);
}



//db.managerextracts.updateMany({manager:'ALAN.BROWN@ZURICH.COM'},{$set:{dmanager:"rambaldhage"}})


//module.exports.updateManagerName = function(oldname, update, callback){
  //  ManagerExtract.updateMany({"manager":oldname},{$set : {"manager":update.manager}});

    //updateMany({"manager":"ACCENTURE@csc.com"},{$set : {"manager":"accenture.csc.com"}})
//}
