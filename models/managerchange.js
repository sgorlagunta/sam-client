const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const ManagerChangeSchema = mongoose.Schema({
  manager: {
    type: String
  },
  comments: {
    type: String
  }
  },
    { versionKey: false });


const ManagerChange = module.exports = mongoose.model('managerchanges', ManagerChangeSchema);

module.exports.getManagerExtracts = function(callback){
    ManagerChange.find(callback);
}
module.exports.addManagers = function(newUploadusers, callback){
    ManagerChange.create(newUploadusers, callback);
}
module.exports.updateManager = function(id, newUploadusers, callback){
    ManagerChange.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.deleteManager = function(id, callback){
    ManagerChange.findByIdAndRemove(id, callback);
}
module.exports.getManager = function(id, callback){
    ManagerChange.findById(id, callback);
}
module.exports.getManagers = function(callback){
    ManagerChange.find(callback);
}
module.exports.deleteManagers = function(callback){
    ManagerChange.remove(callback);
}
module.exports.getUserByUsername = function(manager, callback){
    ManagerChange.find({'manager':manager},callback);
}
//Managerchange.getUserByUsername(username
