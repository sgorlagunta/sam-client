const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');
const Managerchange = require('../models/managerchange');
const managerextract = require('../models/managerextract');

// Register
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  });

  User.addUser(newUser, (err, user) => {
    if(err){
      res.json({success: false, msg:'Failed to register user'});
    } else {
      res.json({success: true, msg:'User registered'});
    }
  });
});

// Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  //User.getUserByUsername(username, (err, user) => {
  console.log("username"+username);
  var uuser = username.toUpperCase();
  console.log("uuser"+uuser);
  managerextract.getUserByUsername(uuser, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found'});
    }

console.log("username"+user);
    const token = jwt.sign({data: uuser}, config.secret, {
      expiresIn: 604800 // 1 week
    });

    res.json({
      success: true,
      token: 'JWT '+token,
      user: {
        id: user._id,
        //manager: manager.name,
        name: user.name,
        username: user.username,
        email: user.email
      }
    });

  /*  User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch){
        const token = jwt.sign({data: user}, config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT '+token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email
          }
        });
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    }); */
  });
});

// Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    res.json({success: true, msg:'Success! You can not see this without a token'});
    //res.json({User: req.user});
});

module.exports = router;
