var express = require('express');
var router = express.Router();
const config = require('../config/database');
const primaryapplications = require('../models/primaryapplications');

router.get('/', function(req, res){
     primaryapplications.getPrimaryApplications(function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
    });
    // return res.json({success: false, msg: 'Wrong password'});
 })
router.post('/', function(req, res){
    var newPrimaryApplications = {
        applicationName: req.body.applicationName,
        applicationOwner : req.body.applicationOwner,
        bu : req.body.bu,
        platform: req.body.platform
    }
     primaryapplications.addPrimaryApplications(newPrimaryApplications,function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
     });
 })
 router.put('/:_id', function(req, res){
     var update = {
        applicationName: req.body.applicationName,
        applicationOwner : req.body.applicationOwner,
        bu : req.body.bu,
        platform: req.body.platform
    }
     primaryapplications.updatePrimaryApplications(req.params._id , update, function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
     });
 })
 router.delete('/:_id', function(req, res){

     primaryapplications.deletePrimaryApplications(req.params._id ,  function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
     });
 })
 router.get('/:_id', function(req, res){

     primaryapplications.getPrimaryApplication(req.params._id , function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
     });
 })

module.exports = router;
