var express = require('express');
var router = express.Router();
const config = require('../config/database');
const managerextract = require('../models/managerextract');
const managerchange = require('../models/managerchange');
const path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  console.log("serveR: set header is done  ");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var DIR = './uploads/';

router.use(bodyParser.json());
var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
  }
});
var upload = multer({ //multer settings
  storage: storage
}).single('photo');

//define the type of upload multer would be doing and pass in its destination, in our case, its a single file with the name photo
//var upload = multer({dest: DIR}).single('single');

//router.get('/', function(req, res, next) {
// render the index page, and pass data to it.
//  res.render('index', { title: 'Express' });
//});

router.get('/', function(req, res){

//  console.log("from server::"+manager);

console.log("from server");

  managerchange.deleteManagers(function(err,managers){
      if(err) throw err;
    });


    managerextract.getManagers(function(err,managers){
        if(err) throw err;
        console.log("Get ManagersList::",+managers.length);

        var manager= {};
        for(var i = 0; i < managers.length; i++){
          //  console.log(data[i].Manager);
          var newManager = {
            manager: managers[i]
          }
          managerchange.addManagers(newManager,function(err,newManager){
            //console.log("added manager");
            if(err) throw err;
          });

        }
     })



     managerextract.getManagerExtracts(function(err,primarymanagers){
         if(err) throw err;
         res.json(primarymanagers);
    });
    // return res.json({success: false, msg: 'Wrong password'});
 });

//our file upload function.
router.post('/', function (req, res, next) {
     var path = '';

     upload(req, res, function (err) {
        if (err) {
          // An error occurred when uploading
          console.log(err);
          return res.status(422).send("an Error occured")
        }
       // No error occured.
        path = req.file.path;
        //return res.send("Upload Completed for "+path);

        /** Multer gives us file info in req.file object */
        if(!req.file){
          res.json({error_code:1,err_desc:"No file passed"});
          return;
        }

            /** Check the extension of the incoming file and
        *  use the appropriate module
        */
        if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
          exceltojson = xlsxtojson;
        } else {
          exceltojson = xlstojson;
        }
        console.log(req.file.path);

        try {
          exceltojson({
            input: req.file.path,
            output: null, //since we don't need output.json
            lowerCaseHeaders:true
          }, function(err,result){
            if(err) {
              return res.json({error_code:1,err_desc:err, data: null});
            }
            var XLSX = require('xlsx');
        var workbook = XLSX.readFile(req.file.path);
        var sheet_name_list = workbook.SheetNames;
        sheet_name_list.forEach(function(y) {
          var worksheet = workbook.Sheets[y];
          var headers = {};
          var data = [];
          for(z in worksheet) {
            if(z[0] === '!') continue;
            //parse out the column, row, and value
            var col = z.substring(0,1);
            var row = parseInt(z.substring(1));
            var value = worksheet[z].v;

            //store header names
            if(row == 1) {
              headers[col] = value;
              continue;
            }

            if(!data[row]) data[row]={};
            data[row][headers[col]] = value;
          }
          //drop those first two rows which are empty
          data.shift();
          data.shift();
          console.log(data);
          console.log(data.length);

          for(var i = 0; i < data.length; i++){
            //  console.log(data[i].Manager);
            var newManager = {
              manager: data[i].Manager,
              resource : data[i].Surname,
              initials : data[i].Initials,
              logonid: data[i].LogonId,
              region: data[i].UserLocation,
              application: data[i].Application,
              status: "Not Recertified",
              comments: ""
            }
            managerextract.addManagers(newManager,function(err,newManager){
              if(err) throw err;
            });
                //console.log('saved record'+i);
              }
            });
            // res.json({error_code:0,err_desc:null, data: result});
          });
        //  res.json({error_code:0,err_desc:null, data: result});
        } catch (e){
          res.json({error_code:1,err_desc:"Corupted excel file"});
        }
  });
return res.json({success: true, msg:'Upload Completed'});
});

router.delete('/:_id', function(req, res){

    managerextract.deleteManager(req.params._id ,  function(err,primarymanagers){
        if(err) throw err;
        res.json(primarymanagers);
    });
});

router.get('/:_id', function(req, res){
console.log("from server::"+manager);
    managerextract.getManager(req.params._id , function(err,primarymanagers){
        if(err) throw err;
        res.json(primarymanagers);
    });
});

router.get('/:manager', function(manager,req, res){
  console.log("from server::"+manager);

    managerextract.getManager(req.params._id , function(err,primarymanagers){
        if(err) throw err;
        res.json(primarymanagers);
    });
});

router.put('/:_id', function(req, res){

   var update = {
     manager: req.body.manager,
     resource : req.body.resource,
     initials : req.body.initials,
     logonid: req.body.logonid,
     region: req.body.region,
     application: req.body.application
   }


    managerextract.updateManager(req.params._id , update, function(err,primarymanagers){
        if(err) throw err;
        res.json(primarymanagers);
    });
});


/* router.get('/', function(manager,req, res){

    managerextract.getManagerExtractsClients(manager, function(err,primarymanagers){
        if(err) throw err;
        res.json(primarymanagers);
    });
}); */

module.exports = router;
