var express = require('express');
var router = express.Router();
const config = require('../config/database');
const managerchange = require('../models/managerchange');
const managerextract = require('../models/managerextract');

router.get('/:_id', function(req, res){
    managerchange.getManager(req.params._id , function(err,manager){
        if(err) throw err;
        res.json(manager);
    });
});



router.get('/', function(req, res){
      managerchange.getManagers(function(err,listmanagers){
          if(err) throw err;
          //console.log(listmanagers);
         // console.log("ManagersList new table::",+managers.length);
           res.json(listmanagers);
         // console.log("ManagersList new table::",+managers.length);
        });
         //console.log("saved managers into new table");
    //return res.json({success: true, msg:'Managers List'});
 });


 router.put('/:_id', function(req, res){

    var oldname;
  console.log("Update::");
    managerchange.getManager(req.params._id , function(err,manager){
        if(err) throw err;
        //res.json(manager);
        oldname = manager.manager;

        managerextract.getManagersName(oldname,function(err,managers){
            if(err) throw err;
          //  res.json(primarymanagers);

            //console.log("managerextract::"+manager);
            //res.json(primarymanagers);

            //managers.manager = req.body.manager;
            //console.log("managers length::"+managers.length);
            for(var i=0;i<managers.length;i++)
            {
              //console.log("in loop::"+managers[i]._id);
              managers[i].manager = req.body.manager;
              var update = {
                manager: managers[i].manager,
                surname : managers[i].surname,
                initials : managers[i].initials,
                logonid: managers[i].logonid,
                region: managers[i].region,
                application: managers[i].application
              }

              managerextract.updateManagerName(managers[i]._id, update,function(err,manager){
                  if(err) throw err;
                  //res.json(primarymanagers);
                });
            }


        });
    });

    var update2 = {
      manager: req.body.manager
    }


        console.log("new::"+req.body.manager);
     managerchange.updateManager(req.params._id , update2, function(err,manager){
         if(err) throw err;
         //res.json(manager);
         return res.json({success: true, msg:'Manager updated successfully'});
     });


 });
module.exports = router;
