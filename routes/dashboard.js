var express = require('express');
var router = express.Router();
const config = require('../config/database');
const dashboard = require('../models/managerextract');
const managerchange = require('../models/managerchange');
const path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

router.get('/', function(req, res){
  dashboard.getUsersDashboard(function(err,usersDashboard){
      if(err) throw err;
      res.json(usersDashboard);
 });
});
module.exports = router;
