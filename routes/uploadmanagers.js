var express = require('express');
var router = express.Router();
const config = require('../config/database');
const primarymanagers = require('../models/uploadmanagers');
const path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  console.log("serveR: set header is done  ");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/** Serving from the same express Server
No cors required */
//router.use(express.static(path.join(__dirname, 'public')));
router.use(bodyParser.json());
var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
  }
});
var upload = multer({ //multer settings
  storage: storage
}).single('file');

router.post('/', function(req, res) {
    console.log("Calling post menthod:");
    upload(req,res,function(err){
    if(err){
      res.json({error_code:1,err_desc:err});
      return;
    }

    /** Multer gives us file info in req.file object */
    if(!req.file){
      res.json({error_code:1,err_desc:"No file passed"});
      return;
    }
    /** Check the extension of the incoming file and
    *  use the appropriate module
    */
    if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    console.log(req.file.path);




    try {
      exceltojson({
        input: req.file.path,
        output: null, //since we don't need output.json
        lowerCaseHeaders:true
      }, function(err,result){
        if(err) {
          return res.json({error_code:1,err_desc:err, data: null});
        }


        var XLSX = require('xlsx');
        var workbook = XLSX.readFile(req.file.path);
        var sheet_name_list = workbook.SheetNames;
        sheet_name_list.forEach(function(y) {
          var worksheet = workbook.Sheets[y];
          var headers = {};
          var data = [];
          for(z in worksheet) {
            if(z[0] === '!') continue;
            //parse out the column, row, and value
            var col = z.substring(0,1);
            var row = parseInt(z.substring(1));
            var value = worksheet[z].v;

            //store header names
            if(row == 1) {
              headers[col] = value;
              continue;
            }

            if(!data[row]) data[row]={};
            data[row][headers[col]] = value;
          }
          //drop those first two rows which are empty
          data.shift();
          data.shift();
          console.log(data);
          console.log(data.length);

          for(var i = 0; i < data.length; i++){
            //  console.log(data[i].Manager);
            var newManager = {
              manager: data[i].Manager,
              resource: data[i].Surname,
              initials : data[i].Initials,
              logonid : data[i].LogonId,
              userlocation : data[i].UserLocation,
              application: data[i].Application
            }
            primarymanagers.addUploadmanagers(newManager,function(err,newManager){
              if(err) throw err;
            });
            //console.log('saved record'+i);
          }
        });
        // res.json({error_code:0,err_desc:null, data: result});
      });
    } catch (e){
      res.json({error_code:1,err_desc:"Corrupted excel file"});
    }
  })
//res.redirect('/uploaduser');
});

router.get('/', function(req, res) {
  //res.json({error_code:0,err_desc:null});
    console.log("serveR: calling get method ");
  //  return res.json("primaryusers:Get method");
  primarymanagers.getUploadmanagers(function(err,primarymanagers){
  if(err) throw err;
    console.log("getUploadmanagers:"+primarymanagers);
    return res.json(primarymanagers);
  });
});


module.exports = router;
